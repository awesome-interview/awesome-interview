---
title: Go
subtitle: Співбесіда з Go. 200+ запитань для Junior, Middle, Senior
date: 2022-06-19
tags: ["go"]
---

### Junior
#### Software engineering

1. Які бувають області пам’яті програми? У чому їхні особливості та відмінності?
2. Розкажіть, що ви знаєте про HTTP-протокол, які складові частини запиту? Які статус-коди знаєте, які групи можна виділити?
3. Назвіть та опишіть будь-який патерн програмування (на ваш вибір). Коли його доцільно використовувати та чому?
4. Що таке процеси та потоки в операційній системі? Опишіть їхній взаємозв’язок у контексті виконання програми.
5. Що таке шаблон проєктування (design pattern)?
6. Які знаєте алгоритми сортування?
7. Що таке Big-O notation?
8. Які відмінності між unit-тестом та інтеграційним тестом?
9. Що таке mock і для чого його використовують?
10. Що таке SDLC? Які етапи й навіщо вони потрібні?
11. Що таке SOLID?
12. Що таке MVC?
13. Що таке DBMS? Яка різниця між СУБД та базою даних?
14. Які бувають типи баз даних?
15. Що таке Docker?

------

#### Бази даних
16. Опишіть ACID-властивості реляційних баз даних.
17. Є таблиця в реляційній БД зі 100 тисячами рядків. SELECT-запит з такої таблиці триває час t. Як зміниться час запиту, якщо кількість рядків буде один мільйон? Які є варіанти, залежно від запиту/таблиці?
18. Для чого використовують індекси в базах даних і завдяки чому (яким структурам даних) вони працюють?
19. З якими NoSQL-базами даних працювали? Які їхні переваги над реляційними базами?

#### Go
20. На вашу думку, у чому перевага Go перед іншими мовами?
{{<indent-sm>}}

Одна з найкрутіших можливостей Go — це горутини (Goroutines). Завдяки їм, щоб 
виконати частину коду в паралельному потоці, достатньо огорнути його в функцію 
і додати Go на початку. 
Такі горутини можна запускати понад тисячу, а Go Runtime самостійно розподілить 
їх по системних потоках. При цьому є також зручний інструментарій для роботи з 
горутинами, що допомагає їх легко координувати.

Якщо розглянути Go у контексті об’єктноорієнтованого програмування, то вона 
базується на поліморфізмі через інтерфейси (єдина абстракція в Go) та композиції, 
що в Go реалізована як вбудовування, або embedding. Так, у Go немає наслідування, але цьому є розумне пояснення — ті патерни, що реалізовані за допомогою наслідування, також можна імплементувати через embedding. А оскільки філософія Go базується на одному способі розв’язання задачі та одному стилі використання цього способу, то розробники обрали також один принцип — вбудовування.

Ще з цікавого у Go — це інкапсуляція на рівні пакета. У цій мові ми можемо 
оголосити функцію/метод/змінну/структуру приватною на рівні пакета, написавши його 
ім’я маленькими літерами. І навпаки — якщо хочемо зробити публічним, то великими. 
У підсумку маємо лише два ідентифікатори доступу — приватний та публічний. Проте 
згадайте, як часто вам потрібні інші?

Ще одна перевага Go — багата стандартна бібліотека. Для прикладу, 
пакет «net/http» дозволить за кілька рядків коду підняти робочий вебсервіс, 
а в купі з пакетом «html/template» — реалізувати простенький, але повноцінний сайт 
за кілька днів.

{{</indent-sm>}}
21. Що таке горутини й навіщо вони?
{{<indent-sm>}}

Горутини - це ф-ї, які виконуються паралельно (це один з способів виконати ф-ю).
Наприклад, є ф-я **fmt.Println** - ми можемо її просто викликати, а можемо викликати так, щоб
вона виконалась не відразу, а колись пізніше, відкладено.
Горутини легковісні, в кожної з них є свій стек, все інше - пам"ять, файли - загальне.
Горутини дозволяють запускати будь-яку кількість дій одночасно.

When a Go program starts up, the Go runtime asks the machine (virtual or physical)
how many operating system threads can run in parallel. This is based on the number of
cores that are available to the program. For each thread that can be run in parallel,
the runtime creates an operating system thread (M) and attaches that to a data structure
that represents a logical processor (P) inside the program.
This P and M represent the compute power or execution context for running the Go program.

Also, an initial Goroutine (G) is created to manage the execution of
instructions on a selected M/P. Just like an M manages the execution of instructions
on the hardware, a G manages the execution of instructions on the M. This creates
a new layer of abstraction above the operating system, but it moves execution control
to the application level.

![Горутини](/images/go/goroutine.png)
[Что такое горутины и каков их размер?](https://habr.com/ru/company/otus/blog/527748/)

{{</indent-sm>}}

23. Коли приостанавлюється горутина?
{{<indent-sm>}}

Горутина призупиняється на очікування чого завгодно, коли виконується якась синхронізація,
якийсь мютекс, канал, waitgroup, коли горутина йде на якийсь системний виклик, наприклад
потрібно щось прочитати з файла, або з мережі, коли горутина просто спить.


{{</indent-sm>}}
24. Що таке GOROOT і GOPATH?
{{<indent-sm>}}

Переменная среды **$GOPATH** перечисляет места, где Go ищет рабочие пространства Go.

По умолчанию Go использует в качестве GOPATH расположение **$HOME/go**,
где **$HOME** — корневой каталог учетной записи пользователя нашего компьютера. 
Для изменения этой настройки следует изменить переменную среды **$GOPATH**.

**$GOPATH — это не $GOROOT**

Переменная **$GOROOT** определяет расположение кода Go, 
компилятора и инструментов, а не нашего исходного кода.

**$GOROOT** это переменная, указывающая, где лежит, собственно, вся бинарная сборка Go 
и исходные коды. Что-то вроде JAVA_HOME. 
Устанавливать эту переменную ручками нужно только в тех случаях, 
если вы ставите Go под Windows не с помощью MSI-инсталлера, а из zip-архива. 
Ну, или если вы хотите держать несколько версий Go, каждая в своей директории.

Переменная **$GOROOT** обычно имеет значение вида **/usr/local/go**.

Переменная **$GOPATH** обычно имеет значение вида **$HOME/go**.
GOPATH=/Users/myuser/go

Хотя явно задавать переменную **$GOROOT** больше не нужно, 
она все еще упоминается в старых материалах.

Раньше (до Go 1.0) эта переменная была нужна — её использовали скрипты сборки, равно как и GOARCH и GOOS. 
Но после Go 1.0 немного изменилась внутренняя логика работы go tool и сейчас значение 
GOROOT хардкодится на этапе сборки или инсталляции. Тоесть, go — дефолтно проинсталлированный 
— знает это значение и так. Его можно посмотреть с помощью команды:

```
go env GOROOT
```

В MacOS X это /usr/local/go/, \
в Linux тоже (хотя может зависеть от дистрибутива), в Windows — С:\Go.

А вот переменная GOPATH очень важна и нужна и её нужно установить обязательно, 
впрочем только один раз. Это ваш workspace, где будет лежать код и бинарные файлы всего 
с чем вы будете в Go работать. 
Поэтому выбирайте удобный вам путь и сохраняйте его в GOPATH. К примеру:

```
export GOPATH=~/go
export GOPATH=~/gocode
export GOPATH=~/Devel/go
export GOPATH=~/Projects/go
```

Ну и обязательно это сохраните в .profile или как вы там сохраняете переменные:

```
echo "export GOPATH=~/go" >> ~/.profile (или .bash_profile)
```

Всё, сделайте это один раз и забудьте. Создайте только эту директорию, если её еще и нет, 
и на этом готово.\
Теперь любой вызов **go get github.com/someuser/somelib** автоматически будет скачивать 
исходники в **$GOPATH/src**, а бинарный результат компиляции складывать 
в **$GOPATH/pkg** или **$GOPATH/bin** (для библиотек и исполняемых файлов соответственно).

{{</indent-sm>}}
25. Які типи даних використовуються в Go?
{{<indent-sm>}}
Types provide integrity and readability by asking 2 questions:
- What is the amount of memory to allocate? (e.g. 1, 2, 4, 8 bytes)
- What does that memory represent? (e.g. int, uint, bool,..)

Types can be specific to a precision such as int32 or int64:
- uint8 represents an unsigned integer with 1 byte of allocation
- int32 represents a signed integer with 4 bytes of allocation

When I declare a type using a non-precision based type (unit, int) the size 
of the value is based on the architecture being used to build the program:
- 32 bit arch: int represents a signed int at 4 bytes of memory allocation
- 64 bit arch: int represents a signed int at 8 bytes of memory allocation

**Word Size**\
The word size represents the amount of memory allocation required to store integers and pointers for a given architecture. For example:
- 32 bit arch: word size is 4 bytes of memory allocation
- 64 bit arch: word size is 8 bytes of memory allocation

This is important because Go has internal data structures (maps, channels, slices, 
interfaces, and functions) that store integers and pointers. The size of these data 
structures will be based on the architecture being used to build the program.

In Go, the amount of memory allocated for a value of type int, a pointer, 
or a word will always be the same on the same architecture.

**Zero Value Concept**\
Every single value I construct in Go is initialized at least to its zero value state unless 
I specify the initialization value at construction. 
The zero value is the setting of every bit in every byte to zero.

This is done for data integrity and it’s not free. It takes time to push 
electrons through the machine to reset those bits, but I should always take 
integrity over performance.

```
Type       Zero Value

Boolean    false
Integer    0
Float      0
Complex    0i
String     "" (empty)
Pointer    nil
```

**Базові типи**
```
bool
string
int  int8  int16  int32  int64
uint uint8 uint16 uint32 uint64 uintptr
byte // псевдонім для uint8
rune // псевдонім int32, представляє точку коду Юнікоду
float32 float64
complex64 complex128
```

**Псевдоніми типів**\
В Go ви можете використовувати псевдоніми для створення різноманітних типів, 
але не можете присвоїти значення базового типу до псевдоніма без перетворення. 
Наприклад, присвоєння **var b int = a** 
у наступній програмі викликає помилку компіляції, тому що тип Age є псевдонімом int, 
але сам він не являється int:

```
package main
 
type Age int
 
func main() {
    var a Age = 5
    var b int = a
}
 
Вивід:
 
tmp/sandbox547268737/main.go:8: cannot use a (type Age) as
type int in assignment
```

Ви можете згрупувати оголошення типів або використовувати одне оголошення на рядок:
```
type IntIntMap map[int]int
StringSlice []string
 
type (
    Size   uint64
    Text  string
    CoolFunc func(a int, b bool)(int, error)
)
```

**Рядки**\
Рядки в Go закодовані в UTF8, і тому можуть представляти будь-який символ Юнікоду. 
Пакет strings надає багато операцій з рядками. Ось приклад 
взяття масиву рядків, приведення їх до правильного регістру та приєднання до речення.

```
package main
 
import (
    "fmt"
    "strings"
)
 
func main() {
    words := []string{"i", "LikE", "the ColORS:", "RED,", 
                      "bLuE,", "AnD", "GrEEn"}
    properCase := []string{}
     
    for i, w := range words {
        if i == 0 {
            properCase = append(properCase, strings.Title(w))
        } else {
            properCase = append(properCase, strings.ToLower(w))
        }
    }
     
    sentence := strings.Join(properCase, " ") + "."
    fmt.Println(sentence)
}
```

**Вказівники**\
У Go є вказівники. Нульовий вказівник дорівнює nil. 
Ви можете отримати вказівник на значення, використовуючи оператор & й повернути, 
використовуючи оператор *. У вас також можуть бути вказівники на інші вказівники.

```
package main
 
import (
    "fmt"
)
 
 
type S struct {
    a float64
    b string
}
 
func main() {
    x := 5
    px := &x
    *px = 6
    fmt.Println(x)
    ppx := &px
    **ppx = 7
    fmt.Println(x)
}
```

**Об'єктно-орієнтоване програмування**\
Go підтримує об'єктно-орієнтоване програмування через інтерфейси та структури. 
Немає ні класів, ні ієрархії класів, хоча ви можете вставляти анонімні структури всередину 
структур, що забезпечує певне одинарне наслідування.

**Інтерфейси**\
Інтерфейси є наріжним каменем системи типів Go. Інтерфейс ー лише колекція сигнатур методів. 
Кожен тип, який реалізує всі методи, є сумісним з інтерфейсом. Ось короткий приклад. 
Інтерфейс Shape визначає два методи: GetPerimeter() та GetArea(). 
Об'єкт Square реалізує інтерфейс.

```
type Shape interface {
    GetPerimeter() uint
    GetArea() uint
}
 
type Square struct {
   side  uint
}
 
func (s *Square) GetPerimeter() uint {
    return s.side * 4
}
 
func (s *Square) GetArea() uint {
    return s.side * s.side
}
```
Порожній інтерфейс **interface{}** сумісний з будь-яким типом, 
оскільки не має ніяких необхідних методів. Порожній інтерфейс потім може вказувати 
на будь-який об'єкт (аналогічно до Object у Java або до вказівника void у C/C++) 
та часто використовується для динамічної типізації. Інтерфейси завжди є вказівниками 
й завжди вказують на конкретний об'єкт.

**Структури (Structs)**\
Структури ー користувацькі типи Go. Структура містить іменовані поля, які можуть 
бути базовими типами, типами вказівників або іншими типами структур. Ви також можете 
анонімно вставляти структури в інші структури як одну з форм наслідування реалізації.

У наступному прикладі структури S1 та S2 вставлені в структуру S3, яка 
також має своє власне поле int та вказівник на свій власний тип:

```
package main
 
import (
    "fmt"
)
 

type S1 struct {
    f1 int
}
 
type S2 struct {
    f2 int
}
 
type S3 struct {
    S1
    S2
    f3 int
    f4 *S3
}
 
func main() {
    s := &S3{S1{5}, S2{6}, 7, nil}
     
    fmt.Println(s)
}

Вивід:
 
&{{5} {6} 7 <nil>}
```

**Твердження типу (Type Assertions)**\
Твердження типу дозволяє вам перетворити інтерфейс у його конкретний тип. 
Якщо ви вже знаєте базовий тип, то просто можете затвердити його. 
А якщо ви невпевнені ー можете спробувати декілька тверджень типу, поки не знайдете правильний.

У наступному прикладі представлений список елементів, що містять рядкові 
та не рядкові значення, представлені у вигляді зрізу порожніх інтерфейсів. 
Код виконує ітерацію по всім речам, намагаючись перетворити кожен елемент в 
рядок й зберегти всі рядки в окремому зрізі, який він в результаті виводить.

```
package main
 
import "fmt"
 
func main() {
    things := []interface{}{"hi", 5, 3.8, "there", nil, "!"}
    strings := []string{}
     
    for _, t := range things {
        s, ok := t.(string)
        if ok {
            strings = append(strings, s)
        }
    }
     
    fmt.Println(strings)
     
}
 
Вивід:
 
[hi there !]
```

**Рефлексія (Reflection)**\
Пакет Go reflect дозволяє відразу перевіряти тип інтерфейсу без тверджень типу. 
Ви також можете отримати значення інтерфейсу й за бажанням конвертувати його в інтерфейс 
(не так корисно).

Ось приклад, аналогічний минулому, але замість виводу рядків, він просто їх 
підраховує, тому нема потреби перетворювати interface{} у string. 
Ключовим являється виклик reflect.Type() для отримання типу об'єкта, 
який має метод Kind(), що дозволяє нам визначати, з рядком ми маємо справу, чи ні.

```
package main
 
import (
    "fmt"
    "reflect"
)
 
 
func main() {
    things := []interface{}{"hi", 5, 3.8, "there", nil, "!"}
    stringCount := 0
     
    for _, t := range things {
        tt := reflect.TypeOf(t)
        if tt != nil && tt.Kind() == reflect.String {
            stringCount++
        }
    }
     
    fmt.Println("String count:", stringCount)
}
```

**Функції**\
Функції ー об'єкти першого класу в Go. Це означає, що ви можете призначати функції змінним, 
передавати функції в якості аргументів іншим функціям або повертати їх в якості результатів. 
Це дозволяє вам використовувати стиль функціонального програмування з Go.

Наступний приклад демонструє кілька функцій ー GetUnaryOp() та GetBinaryOp(), 
які повертають анонімні функції, обрані випадковим чином. Головна програма вирішує ー 
їй потрібна унарна чи бінарна операція, заснована на кількості аргументів. 
Вона зберігає обрану функцію у локальній змінній під назвою «op», а потім викликає 
її з правильною кількістю аргументів.

```
package main
 
import (
    "fmt"
    "math/rand"
)
 
type UnaryOp func(a int) int
type BinaryOp func(a, b int) int
 
 
func GetBinaryOp() BinaryOp {
    if rand.Intn(2) == 0 {
        return func(a, b int) int { return a + b }
    } else {
        return func(a, b int) int { return a - b }
    }
}
 
func GetUnaryOp() UnaryOp {
    if rand.Intn(2) == 0 {
        return func(a int) int { return -a }
    } else {
        return func(a int) int { return a * a }
    }
}
 
 
func main() {
    arguments := [][]int{{4,5},{6},{9},{7,18},{33}}
    var result int
    for _, a := range arguments {
        if len(a) == 1 {
            op := GetUnaryOp()
            result = op(a[0])            
        } else {
            op := GetBinaryOp()
            result = op(a[0], a[1])                    
        }
        fmt.Println(result)                
    }
}
```

**Канали**\
Канали ー незвичайний тип даних. Ви можете думати про них як про черги повідомлень, 
використовуваних для передачі повідомлень між goroutines. Канали суворо типізовані. 
Вони синхронізовані та мають виділену підтримку синтаксису для надсилання й отримання 
повідомлень. Кожен канал може бути тільки для приймання, тільки для відправлення, 
або двостороннім.

Канали також можуть бути буферизовані. Ви можете перебирати повідомлення у каналі, 
використовуючи діапазон (range), а goroutines можуть блокуватися у декількох каналах 
одночасно за допомогою універсальної операції вибору.

Ось типовий приклад, в якому сума квадратів списку цілих чисел паралельно обчислюється 
двома goroutines, кожна з яких відповідає за половину списку. Головна функція очікує 
результат з обох goroutines й потім додає їх, щоб одержати повну суму. Зверніть увагу 
на те, як за допомогою вбудованої функції make() створюється канал c та на те, як з 
допомогою спеціального оператора <- код читається з каналу та записується у нього.

```
package main
 
import "fmt"
 
func sum_of_squares(s []int, c chan int) {
    sum := 0
    for _, v := range s {
        sum += v * v
    }
    c <- sum // надсилання у с
}
 
func main() {
    s := []int{11, 32, 81, -9, -14}
 
    c := make(chan int)
    go sum_of_squares(s[:len(s)/2], c)
    go sum_of_squares(s[len(s)/2:], c)
    sum1, sum2 := <-c, <-c // отримання з с
    total := sum1 + sum2
 
    fmt.Println(sum1, sum2, total)
}
```

**Колекції**\
Go має вбудовані загальні колекції, що можуть зберігати дані будь-якого типу. 
Ці колекції є спеціальними - ви не можете визначати ваші власні загальні колекції. 
До колекцій належать масиви, зрізи та мапи. Канали також є загальними й також 
можуть вважатися колекціями, але у них є доволі унікальні властивості, тому я 
вважаю за краще обговорювати їх окремо.

**Масиви**\
Масиви — колекції фіксованого розміру, 
що містять елементи одного типу. Ось приклади деяких масивів:

```
package main
import "fmt"
 
 
func main() {
    a1 := [3]int{1, 2, 3}
    var a2 [3]int
    a2 = a1 
 
    fmt.Println(a1)
    fmt.Println(a2)
     
    a1[1] = 7
 
    fmt.Println(a1)
    fmt.Println(a2)
     
    a3 := [2]interface{}{3, "hello"}
    fmt.Println(a3)
}
```

Розмір масиву є частиною його типу. Ви можете копіювати масиви однакового розміру і типу. 
Копіювання відбувається за значенням. Якщо ви хочете зберігати значення іншого типу, 
ви можете використовувати запасний вихід з масиву порожніх інтерфейсів.

**Зрізи (Slices)**\
Масиви доволі обмежені через їх фіксований розмір. Зрізи є більш цікавими. 
Ви можете розглядати зрізи як динамічні масиви. Зрізи використовують масив для 
зберігання своїх елементів. Ви можете перевірити довжину зрізу, додати елементи 
й інші зрізи, і найцікавіше з цього всього ー ви можете витягти суб-зрізи, 
подібно до слайсингу в Python:

```
package main
 
import "fmt"
 
 
 
func main() {
    s1 := []int{1, 2, 3}
    var s2 []int
    s2 = s1 
 
    fmt.Println(s1)
    fmt.Println(s2)
 
    // Modify s1    
    s1[1] = 7
 
    // І s1 й s2 вказують на один і той самий базовий масив
    fmt.Println(s1)
    fmt.Println(s2)
     
    fmt.Println(len(s1))
     
    // Slice s1
    s3 := s1[1:len(s1)]
     
    fmt.Println(s3)
}
```

Коли ви копіюєте зрізи, ви просто копіюєте посилання на той же 
базовий масив. Коли ви робите зріз, суб-зріз, як і раніше, вказує на той самий 
масив. Але коли ви додаєте, то отримуєте зріз, що вказує на новий масив.

Ви можете перебирати масиви чи зрізи, використовуючи звичайний цикл з індексами або діапазони. 
Ви також можете створювати зрізи заданої місткості, які будуть ініціалізовані нульовим 
значенням їх типу даних за допомогою функції make():

```
package main
 
import "fmt"
 
 
 
func main() {
    // Створює зріз на 5 елементів булевого типу, що ініціалізовані як false 
    s1 := make([]bool, 5)
    fmt.Println(s1)
     
    s1[3] = true
    s1[4] = true
 
    fmt.Println("Iterate using standard for loop with index")
    for i := 0; i < len(s1); i++ {
        fmt.Println(i, s1[i])
    }
     
    fmt.Println("Iterate using range")
    for i, x := range(s1) {
        fmt.Println(i, x)
    }
}
 
Вивід:
 
[false false false false false]
Iterate using standard for loop with index
0 false
1 false
2 false
3 true
4 true
Iterate using range
0 false
1 false
2 false
3 true
4 true
```

**Мапи (Maps)**\
Мапи ー колекції пар ключ-значення. 
Ви можете присвоїти їм літерали мап або інші мапи. 
Також ви можете створити порожні мапи, використовуючи вбудовану функцію make. 
Доступ до елементів можна отримати за допомогою квадратних дужок. 
Мапи підтримують ітерування з використанням range, а протестувати, 
чи існує ключ, можна намагаючись отримати доступ до нього й перевіряючи 
друге необов'язкове булеве значення.

```
package main
 
import (
    "fmt"
)
 
func main() {
    // Створення мапи, використовуючи літерал мапи
    m := map[int]string{1: "one", 2: "two", 3:"three"}
     
    // Присвоєння елементу за ключем
    m[5] = "five"
    // Доступ до елементу за ключем
    fmt.Println(m[2])
     
    v, ok := m[4]
    if ok {
        fmt.Println(v)
    } else {
        fmt.Println("Missing key: 4")
    }
     
     
    for k, v := range m {
        fmt.Println(k, ":", v)
    }
}
 
Вивід:
 
two
Missing key: 4
5 : five
1 : one
2 : two
3 : three
```

Зверніть увагу, що ітерація виконується не в порядку створення або вставки.

**Нульові значення**\
У Go немає ніяких неініціалізованих типів. Кожен тип має попередньо визначене нульове значення. Якщо змінна типу оголошується без присвоєння їй значення, тоді вона містить нульове значення. Це важливий елемент безпеки.

Для будь-якого типу T, ***new(T)** поверне одне нульове значення Т.

Для булевих типів нульовим значенням є false. 
Для чисельних типів нульове значення дорівнює ... нулю. 
Для зрізів, мап та вказівників ー порожній покажчик (nil). 
Для структур ー структура, де всі поля ініціалізуються нульовим значенням.

```
package main
 
import (
    "fmt"
)
 
 
type S struct {
    a float64
    b string
}
 
func main() {
    fmt.Println(*new(bool))
    fmt.Println(*new(int))
    fmt.Println(*new([]string))
    fmt.Println(*new(map[int]string))
    x := *new([]string)
    if x == nil {
        fmt.Println("Uninitialized slices are nil")
    }
 
    y := *new(map[int]string)
    if y == nil {
        fmt.Println("Uninitialized maps are nil too")
    }
    fmt.Println(*new(S))
}
```

{{</indent-sm>}}

26. Що робить функція init()? Наведіть приклади, де її варто використовувати. Наведіть приклади, коли варто уникати.
{{<indent-sm>}}

**The init function**\
Finally, each source file can define its own niladic init 
function to set up whatever state is required. 
(Actually each file can have multiple init functions.) 
And finally means finally: init is called after all the variable 
declarations in the package have evaluated their initializers, 
and those are evaluated only after all the imported packages have been initialized.

Besides initializations that cannot be expressed as declarations, 
a common use of init functions is to verify or repair correctness 
of the program state before real execution begins.

```
func init() {
    if user == "" {
        log.Fatal("$USER not set")
    }
    if home == "" {
        home = "/home/" + user
    }
    if gopath == "" {
        gopath = home + "/go"
    }
    // gopath may be overridden by --gopath flag on command line.
    flag.StringVar(&gopath, "gopath", gopath, "override default GOPATH")
}
```

need to use **go tool objdump** the binary file to collect all the init functions.

**disadvantage of init functions**\
It make the code more difficult to read, review and debug:
- it is hard to find all the code that my process have run. You never know that the package sync have a init function run before your main package. I need to use go tool objdump the binary file to collect all the init functions.

It make the compiler harder:
- all the variables that the init function is used can not be dead code elimination even if that variables is not referenced from any other place.

It encourage programer to write slow code:
- cost 10MB memory and 200ms cpu time in the init function in layers.init cost too much time  google/gopacket#101 when 99% of processes of my binary do not use touch any gopacket function.

**advantage of init functions**\
The problem after init function is removed:
- Register something in another package to workaround import circle. Like sync.runtime_registerPoolCleanup in sync package,crypto.RegisterHash in md5 package. This problem can be solved partly by delete those register stuff, and ask caller to use the md5 package.
- Find the cpu information and store it in a variable to speed up other runtime stuff. Like cpuid

В Go заданнная функция init() выделяет элемент кода, который запускатся до 
любой другой части вашего пакета. Этот код запускается сразу же после импорта пакета, 
и его можно использовать при необходимости инициализации приложения в определенном 
состоянии, например, если для запуска приложения требуется определенная конфигурация 
или набор ресурсов. Также используется при импорте побочных эффектов, то есть при 
применении методики установки состояния программы посредством импорта определенного пакета. 
Часто используется для регистрации одного пакета в другом, чтобы программа рассматривала 
правильный код для этой задачи.

Хотя init() представляет собой полезный инструмент, иногда он 
делает код менее удобочитаемым, поскольку трудный в поиске экземпляр init() 
серьезно влияет на порядок выполнения кода. В связи с этим, начинающим разработчикам 
Go важно понимать все аспекты этой функции, чтобы они использовали init() в читаемой 
форме при написании кода.

**Предварительные требования**\
Будет использоваться следующая структура файлов:

```
.
├── bin
│
└── src
    └── github.com
        └── gopherguides
```

**Декларирование init()**

Каждый раз, когда вы декларируете функцию init(), Go загружает и запускает ее прежде всех остальных элементов этого пакета. Чтобы продемонстрировать это, в данном разделе мы подробно покажем определение функции init() и ее влияние на выполнение пакета.

Вначале рассмотрим следующий пример кода без функции init():
```
//main.go

package main

import "fmt"

var weekday string

func main() {
	fmt.Printf("Today is %s", weekday)
}
```

В этой программе мы декларировали глобальную переменную с именем weekday. По умолчанию значение weekday представляет собой пустую строку.

```
go run main.go
```

Поскольку значение weekday пустое, при запуске программы мы увидим следующее:
```
Output
Today is
```

Мы можем заполнить пустую переменную, используя функцию init() для инициализации значения weekday как текущего дня. Добавьте следующие выделенные строки в файл main.go:

```
//main.go

package main

import (
	"fmt"
	"time"
)

var weekday string

func init() {
	weekday = time.Now().Weekday().String()
}

func main() {
	fmt.Printf("Today is %s", weekday)
}
```

В этом коде мы импортировали и использовали пакет time для получения текущего дня недели (Now(). Weekday(). String()), а затем использовали init() для инициализации weekday с этим значением.

Теперь при запуске программы она выводит текущий день недели:
```
Output
Today is Monday
```

Хотя это показывает принцип работы функции init(), гораздо чаще init() используется при импорте пакета. Это может быть полезно, если вам требуется выполнить в пакете определенные задачи по настройке, прежде чем использовать этот пакет.

**Инициализация пакетов при импорте**\
Вначале мы напишем код, который выбирает случайное существо из среза и выводит его. Однако мы не будем использовать init()в начальной программе. Это лучше покажет стоящую перед нами проблему и возможность ее решения с помощью функции init().

Создайте в каталоге src/github.com/gopherguides/ папку с именем creature с помощью следующей команды:

```
mkdir creature
```

Создайте в папке creature файл с именем creature.go:
```
nano creature/creature.go
```

Добавьте в этот файл следующее содержание:

```
package creature

import (
	"math/rand"
)

var creatures = []string{"shark", "jellyfish", "squid", "octopus", "dolphin"}

func Random() string {
	i := rand.Intn(len(creatures))
	return creatures[i]
}
```

Этот файл определяет переменную с именем creatures, для которой в качестве значений инициализирован набор морских существ. Также она имеет экспортируемую функцию Random, которая выводит случайное значение переменной creatures.

Сохраните и закройте этот файл.

Теперь создайте пакет cmd, который мы используем для записи функции main() и вызова пакета creature.

На том же уровне файла, где мы создали папку creature, создайте папку cmd с помощью следующей команды:

```
mkdir cmd
```

Создайте в папке cmd файл с именем main.go:
```
nano cmd/main.go
```

Добавьте в файл следующие строчки:
```
//cmd/main.go

package main

import (
	"fmt"

	"github.com/gopherguides/creature"
)

func main() {
	fmt.Println(creature.Random())
	fmt.Println(creature.Random())
	fmt.Println(creature.Random())
	fmt.Println(creature.Random())
}
```

Здесь мы импортировали пакет creature и использовали в функции main() функцию creature.Random(), чтобы получить случайное существо и вывести его четыре раза.

Теперь у нас написана вся программа. Однако перед запуском этой программы нам также потребуется создать несколько файлов конфигурации, чтобы обеспечить правильную работу нашего кода. Go использует Go Modules для настройки зависимостей пакетов при импорте ресурсов. Эти модули представляют собой файлы конфигурации в каталоге вашего пакета, которые указывают компилятору, откуда импортировать пакеты. Хотя изучение модулей не входит в состав настоящей статьи, мы можем написать несколько строк конфигурации, чтобы этот пример работал локально.

Создайте в каталоге cmd файл с именем go.mod:
```
nano cmd/go.mod
```

После открытия файла добавьте в него следующий код:
```
//cmd/go.mod

module github.com/gopherguides/cmd
 replace github.com/gopherguides/creature => ../creature
```

Сохраните и закройте файл. Затем создайте файл go.mod в каталоге creature:
```
nano creature/go.mod
```

Добавьте в файл следующую строчку кода:
```
// creature/go.mod

module github.com/gopherguides/creature
```
Это говорит компилятору, что созданный нами пакет creature на самом деле является пакетом github.com/gopherguides/creature. Без этого пакет cmd не будет знать, откуда импортировать этот пакет.

Сохраните и закройте файл.

Теперь у вас должны быть следующая структура каталогов и расположение файлов:
```
├── cmd
│   ├── go.mod
│   └── main.go
└── creature
    ├── go.mod
    └── creature.go
```

Мы завершили настройку и теперь можем запустить программу main с помощью следующей команды:
```
go run cmd/main.go
```

Это даст нам следующее:
```
Output
jellyfish
squid
squid
dolphin
```

При запуске этой программы мы получили четыре значения и вывели их. Если мы запустим программу несколько раз, результаты всегда будут одинаковыми, хотя ожидается случайный результат. Это связано с тем, что пакет rand создает псевдослучайные числа, постоянно дающие один и тот же результат для одного и того же начального состояния. Чтобы получить действительно случайное число, мы можем задать начальное случайное число для пакета или задать изменяющийся источник, чтобы при каждом запуске программы состояние было разным. В Go обычно используется текущее время в качестве начального случайного числа для пакета rand.

Поскольку нам нужно, чтобы пакет creature работал с функцией случайных чисел, откроем этот файл:
```
nano creature/creature.go
```

Добавьте в файл creature.go следующие выделенные строки:
```
//creature/creature.go

package creature

import (
	"math/rand"
	"time"
)

var creatures = []string{"shark", "jellyfish", "squid", "octopus", "dolphin"}

func Random() string {
	rand.Seed(time.Now().UnixNano())
	i := rand.Intn(len(creatures))
	return creatures[i]
}
```

В этом коде мы импортировали пакет time и использовали Seed() для использования текущего времени в качестве начального случайного числа. Сохраните и закройте файл.

Теперь при запуске программы мы будем получать действительно случайный результат:
```
go run cmd/main.go
```

```
Output
jellyfish
octopus
shark
jellyfish
```

При каждом запуске программы результаты будут оставаться случайными. Однако эта реализация кода также не идеальна, поскольку при каждом вызове creature.Random() повторно задается начальное случайное число для пакета rand посредством вызова функции rand.Seed(time.Now(). UnixNano()) еще раз. Повторное начальное случайное число может совпадать с предыдущим, если время на внутренних часах не изменилось. Это может вызвать повторы шаблонов случайных чисел или увеличение времени обработки в связи с ожиданием смены времени на часах.

Для решения этой проблемы мы можем использовать функцию init(). Обновим файл creature.go:
```
nano creature/creature.go
```

Добавьте следующие строчки кода:
```
// creature/creature.go

package creature

import (
	"math/rand"
	"time"
)

var creatures = []string{"shark", "jellyfish", "squid", "octopus", "dolphin"}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func Random() string {
	i := rand.Intn(len(creatures))
	return creatures[i]
}
```

Добавление функции init() указывает компилятору, что при импорте пакета creature необходимо запустить функцию init() один раз и задать начальное случайное число для генерирования случайных чисел. Так нам не придется лишний раз выполнять код. Если мы запустим программу, мы по-прежнему будем получать случайные результаты:
```
go run cmd/main.go
```
```
Output
dolphin
squid
dolphin
octopus
```

В этом разделе мы показали, как использование функции init() может обеспечить проведение правильных расчетов или операций инициализации перед использованием пакета. Далее мы рассморим использование нескольких выражений init() в пакете.

**Использование нескольких экземпляров init()**\
В отличие от функции main(), которую можно декларировать только один раз, функцию init() можно декларировать в пакете много раз. Однако при использовании нескольких экземпляров функции init() может быть сложно понять, какой из них имеет приоритет перед другими. В этом разделе мы покажем, как контролировать несколько выражений init().

В большинстве случаев функции init() выполняются в том порядке, в каком они содержатся в программе. Рассмотрим в качестве примера следующий код:
```
//main.go

package main

import "fmt"

func init() {
	fmt.Println("First init")
}

func init() {
	fmt.Println("Second init")
}

func init() {
	fmt.Println("Third init")
}

func init() {
	fmt.Println("Fourth init")
}

func main() {}
```

Если мы запустим программу с помощью следующей команды:
```
go run main.go
```
```
Output
First init
Second init
Third init
Fourth init
```

Обратите внимание, что каждое выражение init() выполняется в том порядке, в каком оно поступает в компилятор. Однако порядок вызова функций init() может быть не так легко определить.

Рассмотрим более сложную структуру пакета, где у нас имеется несколько файлов, для каждого из которых декларирована собственная функция init(). Для иллюстрации мы создадим программу, передающую переменную с именем message и выводящую ее.

Удалите каталоги creature и cmd и их содержимое из предыдущего раздела и замените их следующими каталогами и структурой файлов:
```
├── cmd
│   ├── a.go
│   ├── b.go
│   └── main.go
└── message
    └── message.go
```

Теперь добавим содержимое каждого файла. В файле a.go добавьте следующие строчки:
```
//cmd/a.go

package main

import (
	"fmt"

	"github.com/gopherguides/message"
)

func init() {
	fmt.Println("a ->", message.Message)
}
```

Этот файл содержит одну функцию init(), которая выводит значение message.Message из пакета message.

Добавьте следующие строки в файл b.go:
```
//cmd/b.go

package main

import (
	"fmt"

	"github.com/gopherguides/message"
)

func init() {
	message.Message = "Hello"
	fmt.Println("b ->", message.Message)
}
```

В файле b.go имеется одна функция init(), которая задает для message.Message значение Hello и выводит его.

Создадим файл main.go, который будет выглядеть следующим образом:
```
//cmd/main.go

package main

func main() {}
```

Этот файл ничего не делает, но предоставляет начальную точку для запуска программы.

В заключение создайте файл message.go как показано здесь:
```
//message/message.go

package message

var Message string
```

Наш пакет message декларирует экспортированную переменную Message.

Для запуска программы выполните следующую команду в каталоге cmd:
```
go run *.go
```

Поскольку в папке cmd имеется несколько файлов Go, составляющих пакет main, нам нужно указать компилятору, что все файлы .go в папке cmd должны быть скомпилированы. Использование *.go указывает компилятору на необходимость загрузить все файлы из папки cmd, которые заканчиваются на .go. Если мы отправим команду go run main.go, программа не будет компилироваться, поскольку она не увидит код в файлах a.go и b.go.

Результат будет выглядеть следующим образом:
```
Output
a ->
b -> Hello
```

Согласно спецификации инициализации пакетов в языке Go, при наличии в пакете нескольких файлов они обрабатываются в алфавитном порядке. В связи с этим, когда мы первый раз распечатали message.Message из a.go, значение было пустым. Значение не было инициализировано до запуска функции init() из b.go.

Если бы мы изменили имя файла с a.go на c.go, результат был бы другим:
```
Output
b -> Hello
a -> Hello
```

Теперь компилятор вначале получает b.go и значение message.Message уже инициализировано как Hello при появлении функции init() в c.go.

Такое поведение может вызвать проблемы при выполнении кода. При разработке программного обеспечения имена файлов часто меняются, и, в связи с особенностями функции init(), изменение имен файлов может изменить последовательность обработки функций init(). Это может привести к нежелательному изменению выводимых программой результатов. Чтобы обеспечить стабильное поведение при инициализации, рекомендуется при сборке указывать компилятору несколько файлов из одного пакета в алфавитном порядке. Чтобы обеспечить загрузку всех функций init() по порядку, можно декларировать все эти функции в одном файле. Это предотвратит изменение порядка даже в случае изменения имен файлов.

Помимо обеспечения порядка выполнения функций init(), вам также следует избегать управления состояниями в пакете с помощью глобальных переменных, т. е. переменных, которые доступны во всем пакете. В предыдущей программе переменная message.Message была доступна всему пакету и поддерживала состояние программы. В связи с таким доступом, выражения init() могли изменять переменную и снижать прогнозируемость работы программы. Чтобы избежать этого, попробуйте работать с переменными в контролируемых пространствах с минимальным доступом, обеспечивающим возможность работы программы.

Итак, в одном пакете может быть несколько деклараций init(). Однако это может создать нежелательные эффекты и сделать программу более сложной для чтения или осложнить прогнозирование ее работы. Если не использовать несколько выражений init() или объединять их в одном файле, поведение программы не изменится в случае перемещения файлов или смены имен файлов.

Теперь посмотрим, как функция init() используется для импорта с побочными эффектами.

**Использование init() для побочных эффектов**

Иногда в Go требуется импортировать пакет не ради его содержимого, но ради побочных эффектов, возникающих при импорте пакета. Часто это означает, что в импортируемом коде содержится выражение init(), выполняемое перед любым другим кодом, что позволяет разработчику изменять состояние программы при запуске. Такая методика называется импортированием для побочного эффекта.

Импортирование для побочного эффекта обычно используется для функции регистрации в коде, чтобы пакет знал, какую часть кода нужно использовать вашей программе. Например, в пакете imageфункция image.Decode должна знать, какой формат она пытается декодировать (jpg, png, gif и т. д.), прежде чем ее можно будет выполнить. Для этого можно предварительно импортировать определенную программу с побочным эффектом выражения init().

Допустим, вы пытаетесь использовать image.Decode в файле .png со следующим фрагментом кода:
```
. . .
func decode(reader io.Reader) image.Rectangle {
	m, _, err := image.Decode(reader)
	if err != nil {
		log.Fatal(err)
	}
	return m.Bounds()
}
. . .
```

Программа с этим кодом будет скомпилирована, однако при попытке декодирования изображения png мы получим сообщение об ошибке.

Для устранения этой проблемы нужно предварительно зарегистрировать формат изображения для image.Decode. К счастью, пакет image/png содержит следующее выражение init():
```
//image/png/reader.go

func init() {
	image.RegisterFormat("png", pngHeader, Decode, DecodeConfig)
}
```

Поэтому, если мы импортируем image/png в сниппет для декодировки, функция image.RegisterFormat() пакета image/png будет запущена до любого нашего кода:
```
. . .
import _ "image/png"
. . .

func decode(reader io.Reader) image.Rectangle {
	m, _, err := image.Decode(reader)
	if err != nil {
		log.Fatal(err)
	}
	return m.Bounds()
}
```

Эта функция задаст состояние и зарегистрирует необходимость использования версии png функции image.Decode(). Эта регистрация происходит в качестве побочного эффекта импорта image/png.

Возможно вы заметили пустой идентификатор (_) перед "image/png". Он необходим, потому что Go не позволяет импортировать пакеты, которые не используются в программе. При указании пустого идентификатора значение импорта отбрасывается так, что действует только побочный эффект импорта. Это означает, что хотя мы не вызываем пакет image/png в нашем коде, мы можем импортировать его ради побочного эффекта.

Важно понимать, когда нужно импортировать пакет ради его побочного эффекта. Без надлежащей регистрации существует вероятность, что программа скомпилируется, но не будет правильно работать при запуске. В документации к пакетам стандартной библиотеки декларируется потребность в этом типе импорта. Если вы напишете пакет, который требуется импортировать ради побочного эффекта, вам также следует убедиться, что используемое выражение init() задокументировано, и импортирующие ваш пакет пользователи смогут правильно его использовать.

В этом обучающем руководстве мы узнали, что функция init() загружается до остальной части кода приложения и может выполнять определенные задачи для пакета, в частности, инициализировать желаемое состояние. Также мы узнали, что порядок выполнения компилятором нескольких выражений init() зависит от того, в каком порядке компилятор загружает исходные файлы. Если вы хотите узнать больше о функции init(), ознакомьтесь с официальной документацией Golang или прочитайте дискуссию в сообществе Go об этой функции.

[Документация Golang](https://golang.org/doc/effective_go.html#init) \
[Дискуссия в сообществе Go об этой функции.](https://github.com/golang/go/issues/25885) \
[Определение и вызов функций](https://www.digitalocean.com/community/tutorials/how-to-define-and-call-functions-in-go) \
[Серия статей по программированию на Go.](https://www.digitalocean.com/community/tutorial_series/how-to-code-in-go) \



{{</indent-sm>}}
27. Поясніть різницю між помилкою і панікою.
{{<indent-sm>}}

Library routines must often return some sort of error indication to the caller. 
As mentioned earlier, 
Go's multivalue return makes it easy to return a detailed error description 
alongside the normal return value. It is good style to use this feature to provide 
detailed error information. For example, as we'll see, os.Open doesn't just return a 
nil pointer on failure, it also returns an error value that describes what went wrong.

By convention, errors have type error, a simple built-in interface.

```
type error interface {
    Error() string
}
```

A library writer is free to implement this interface with a richer model 
under the covers, making it possible not only to see the error but also to 
provide some context. As mentioned, alongside the usual *os.File return value, 
os.Open also returns an error value. If the file is opened successfully, 
the error will be nil, but when there is a problem, it will hold an os.PathError:

```
// PathError records an error and the operation and
// file path that caused it.
type PathError struct {
    Op string    // "open", "unlink", etc.
    Path string  // The associated file.
    Err error    // Returned by the system call.
}

func (e *PathError) Error() string {
    return e.Op + " " + e.Path + ": " + e.Err.Error()
}
```

PathError's Error generates a string like this:

```
open /etc/passwx: no such file or directory
```

Such an error, which includes the problematic file name, the operation, 
and the operating system error it triggered, is useful even if printed far 
from the call that caused it; it is much more informative than the plain 
"no such file or directory".

When feasible, error strings should identify their origin, such as by having 
a prefix naming the operation or package that generated the error. For example, 
in package image, the string representation for a decoding error due to an unknown 
format is "image: unknown format".

Callers that care about the precise error details can use a type switch or a 
type assertion to look for specific errors and extract details. For PathErrors 
this might include examining the internal Err field for recoverable failures.

```
for try := 0; try < 2; try++ {
    file, err = os.Create(filename)
    if err == nil {
        return
    }
    if e, ok := err.(*os.PathError); ok && e.Err == syscall.ENOSPC {
        deleteTempFiles()  // Recover some space.
        continue
    }
    return
}
```

The second if statement here is another type assertion. If it fails, ok will be false, 
and e will be nil. If it succeeds, ok will be true, which means the error was of 
type *os.PathError, and then so is e, which we can examine for more information about the error.


**Panic**

The usual way to report an error to a caller is to return an error as 
an extra return value. The canonical Read method is a well-known instance; 
it returns a byte count and an error. But what if the error is unrecoverable? 
Sometimes the program simply cannot continue.

For this purpose, there is a built-in function panic that in effect creates 
a run-time error that will stop the program (but see the next section). 
The function takes a single argument of arbitrary type—often a string—to 
be printed as the program dies. It's also a way to indicate that something 
impossible has happened, such as exiting an infinite loop.

```
// A toy implementation of cube root using Newton's method.
func CubeRoot(x float64) float64 {
    z := x/3   // Arbitrary initial value
    for i := 0; i < 1e6; i++ {
        prevz := z
        z -= (z*z*z-x) / (3*z*z)
        if veryClose(z, prevz) {
            return z
        }
    }
    // A million iterations has not converged; something is wrong.
    panic(fmt.Sprintf("CubeRoot(%g) did not converge", x))
}
```

This is only an example but real library functions should avoid panic. 
If the problem can be masked or worked around, it's always better to let 
things continue to run rather than taking down the whole program. One possible 
counterexample is during initialization: if the library truly cannot set itself up, 
it might be reasonable to panic, so to speak.

```
var user = os.Getenv("USER")

func init() {
    if user == "" {
        panic("no value for $USER")
    }
}
```

**Recover**
When panic is called, including implicitly for run-time errors such as indexing 
a slice out of bounds or failing a type assertion, it immediately stops execution 
of the current function and begins unwinding the stack of the goroutine, running 
any deferred functions along the way. If that unwinding reaches the top of the 
goroutine's stack, the program dies. However, it is possible to use the built-in 
function recover to regain control of the goroutine and resume normal execution.

A call to recover stops the unwinding and returns the argument passed to panic. 
Because the only code that runs while unwinding is inside deferred functions, 
recover is only useful inside deferred functions.

One application of recover is to shut down a failing goroutine inside a server 
without killing the other executing goroutines.

```
func server(workChan <-chan *Work) {
    for work := range workChan {
        go safelyDo(work)
    }
}

func safelyDo(work *Work) {
    defer func() {
        if err := recover(); err != nil {
            log.Println("work failed:", err)
        }
    }()
    do(work)
}

```

In this example, if do(work) panics, the result will be logged and the goroutine 
will exit cleanly without disturbing the others. There's no need to do anything 
else in the deferred closure; calling recover handles the condition completely.

Because recover always returns nil unless called directly from a deferred function, 
deferred code can call library routines that themselves use panic and recover without 
failing. As an example, the deferred function in safelyDo might call a logging function 
before calling recover, and that logging code would run unaffected by the panicking state.

With our recovery pattern in place, the do function (and anything it calls) 
can get out of any bad situation cleanly by calling panic. We can use that idea 
to simplify error handling in complex software. Let's look at an idealized version of 
a regexp package, which reports parsing errors by calling panic with a local error type. 
Here's the definition of Error, an error method, and the Compile function.

```
// Error is the type of a parse error; it satisfies the error interface.
type Error string
func (e Error) Error() string {
    return string(e)
}

// error is a method of *Regexp that reports parsing errors by
// panicking with an Error.
func (regexp *Regexp) error(err string) {
    panic(Error(err))
}

// Compile returns a parsed representation of the regular expression.
func Compile(str string) (regexp *Regexp, err error) {
    regexp = new(Regexp)
    // doParse will panic if there is a parse error.
    defer func() {
        if e := recover(); e != nil {
            regexp = nil    // Clear return value.
            err = e.(Error) // Will re-panic if not a parse error.
        }
    }()
    return regexp.doParse(str), nil
}
```

If doParse panics, the recovery block will set the return value to 
nil—deferred functions can modify named return values. It will then check, 
in the assignment to err, that the problem was a parse error by asserting that 
it has the local type Error. If it does not, the type assertion will fail, causing 
a run-time error that continues the stack unwinding as though nothing had interrupted it. 
This check means that if something unexpected happens, such as an index out of bounds, 
the code will fail even though we are using panic and recover to handle parse errors.

With error handling in place, the error method (because it's a method bound to a type, 
it's fine, even natural, for it to have the same name as the builtin error type) makes 
it easy to report parse errors without worrying about unwinding the parse stack by hand:

```
if pos == 0 {
    re.error("'*' illegal at start of expression")
}
```

Useful though this pattern is, it should be used only within a package. 
Parse turns its internal panic calls into error values; it does not expose panics 
to its client. That is a good rule to follow.

By the way, this re-panic idiom changes the panic value if an actual error occurs. 
However, both the original and new failures will be presented in the crash report, 
so the root cause of the problem will still be visible. Thus this simple re-panic 
approach is usually sufficient—it's a crash after all—but if you want to display 
only the original value, you can write a little more code to filter unexpected problems 
and re-panic with the original error. That's left as an exercise for the reader.


{{</indent-sm>}}
28. *Поясніть різницю між помилкою і панікою.
{{<indent-sm>}}

Ошибки, возникающие в программе, относятся к двум широким категориям: ожидаемые программистом ошибки и ошибки, возникновения которых не ожидалось. Интерфейс error, который мы изучили в двух предыдущих статьях, посвященных обработке ошибок, в значительной степени имеет дело с ошибками, которые мы ожидаем при написании программ Go. Интерфейс error позволяет признать редкую возможность возникновения ошибок при вызове функций, чтобы мы могли реагировать надлежащим образом на подобные ситуации.

Паники относятся ко второй категории ошибок, которые не ожидались программистом. Эти непредвиденные ошибки приводят к неожиданному прекращению работы и закрытию программы Go. Как правило, возникновение паник обычно вызвано стандартными ошибками. В этом обучающем руководстве мы изучим несколько способов, с помощью которых стандартные операции могут генерировать паники в Go, и рассмотрим способы, позволяющие избежать подобного развития событий. Также мы используем операторы defer вместе с функцией recover для перехвата паник, прежде чем у них появится возможность вызвать неожиданное прекращение работы запущенных программ Go.

В Go есть операции, которые автоматически возвращают паники и завершают работу программы. К таким стандартным операциям относятся индексация массива за пределами его емкости, утверждения типов, вызов методов с указателем nil, некорректное использование мьютексов и попытки работы с закрытыми каналами. Большинство из этих ситуаций обусловлено ошибками во время программирования, которые компилятор не может выявить при компиляции вашей программы.

Поскольку паники содержат информацию, которая может быть полезна для решения проблемы, разработчики обычно использует паники в качестве сигнала о том, что они совершили ошибку во время разработки программы.

**Паники при выходе за границы**\
Когда вы попытаетесь получить доступ к индексу, выходящему за пределы длины среза или массива, Go будет генерировать панику.

В следующем примере мы допустим стандартную ошибку, попробуя получить доступ к последнему элементу среза с помощью длины среза, возвращаемой встроенной функцией len. Попробуйте запустить этот код, чтобы посмотреть, почему это может генерировать панику:

```
package main

import (
	"fmt"
)

func main() {
	names := []string{
		"lobster",
		"sea urchin",
		"sea cucumber",
	}
	fmt.Println("My favorite sea creature is:", names[len(names)])
}
```

```
Output
panic: runtime error: index out of range [3] with length 3

goroutine 1 [running]:
main.main()
	/tmp/sandbox879828148/prog.go:13 +0x20
```

Имя паники в выводе предоставляет подсказку: panic: runtime error: index out of range. Мы создали срез с тремя морскими существами. Затем мы попытались получить последний элемент среза посредством индексации этого среза с помощью длины среза, используя встроенную функцию len. Необходимо помнить, что срезы и массивы ведут отчет с нуля, т. е. первый элемент будет иметь индекс ноль, а последний элемент этого среза — индекс 2. Мы пытаемся получить доступ к элементу среза с индексом 3, однако в срезе нет такого элемента, потому что он находится за пределами среза. У среды для выполнения нет других вариантов, кроме как прекращать работу и осуществлять выход, поскольку мы попросили сделать что-то невозможное. Также Go не может проверить во время компиляции, что этот код будет пытаться сделать это, поэтому с помощью компилятора нельзя поймать эту ошибку.

Обратите внимание также, что весь последующий код не был запущен. Это объясняется тем, что паника — это событие, которое приводит к полному прекращению выполнения вашей программы Go. Полученное сообщение содержит несколько элементов, которые помогают диагностировать причину паники.

**Структура паники**\
Паники состоят из сообщения, указывающего причину паники, и трассировки стека, помогающего локализовать место в вашем коде, где была сгенерирована паника.

Первая часть любой паники — это сообщение. Оно всегда будет начинаться со строки panic:, за которой следует строка, которая изменяется в зависимости от причины паники. Паника из предыдущего упражнения содержит следующее сообщение:

```
panic: runtime error: index out of range [3] with length 3
```

Строка runtime error:​​​, идущая после префикса panic:, указывает, что паника была сгенерирована средой выполнения языка программирования. Эта паника указывает, что мы пытались использовать индекс [3], который выходит из границы длины среза, равной 3.

За этим сообщением следует трассировка стека. Трассировка стека представляет собой карту, которую мы можем использовать для определения точного расположения строки кода, которая выполнялась в момент генерации паники, а также того, как этот код использовался более ранним кодом.

```
goroutine 1 [running]:
main.main()
	/tmp/sandbox879828148/prog.go:13 +0x20
```

Эта трассировка стека из предыдущего примера показывает, что наша программа сгенерировала панику из файла /tmp/sandbox879828148/prog.go на строке номер 13. Кроме того, она указывает нам, что эта паника была сгенерирована в функции main() из пакета main.

Трассировка стека разбита на отдельные блоки — один для каждой goroutine в вашей программе. Каждое исполнение программы Go производится одной или несколькими гоурутинами, каждая из которых может самостоятельно и одновременно выполнять части вашего кода Go. Каждый блок начинается с заголовка goroutine X [state]:. Заголовок предоставляет номер идентификатора гоурутины и состояние, в котором она находилась при возникновении паники. После заголовка трассировка стека показывает функцию, выполняемую программой во время генерации паники, а также имя файла и номер строки, где исполняется функция.

Паника в предыдущем примере была сгенерирована при выходе за границы среза. Паники также могут генерироваться, когда методы вызываются с помощью указателей, которые не заданы.

**Ссылочные типы со значением nil**\
Язык программирования Go имеет указатели, относящиеся к определенным экземплярам определенного типа, которые существуют в памяти компьютера во время исполнения. Указатели могут иметь значение nil, указывающее, что они не указывают ни на что. Когда мы пытаемся вызвать методы с помощью указателя со значением nil, среда выполнения Go будет генерировать панику. Аналогично переменные, хранящие типы интерфейсов, также будут генерировать паники при вызове методов с их помощью. Чтобы увидеть паники, сгенерированные в таких случаях, воспользуйтесь следующим примером:

```
package main

import (
	"fmt"
)

type Shark struct {
	Name string
}

func (s *Shark) SayHello() {
	fmt.Println("Hi! My name is", s.Name)
}

func main() {
	s := &Shark{"Sammy"}
	s = nil
	s.SayHello()
}
```

Получаемые в этом случае паники будут выглядеть следующим образом:

```
Output
panic: runtime error: invalid memory address or nil pointer dereference
[signal SIGSEGV: segmentation violation code=0xffffffff addr=0x0 pc=0xdfeba]

goroutine 1 [running]:
main.(*Shark).SayHello(...)
	/tmp/sandbox160713813/prog.go:12
main.main()
	/tmp/sandbox160713813/prog.go:18 +0x1a
```

В данном примере мы определили структуру с именем Shark. Shark имеет один метод, определенный для указателя получателя с именем SayHello, который будет выводить приветствие при поступлении запроса. Внутри тела функции main мы создаем новый экземпляр структуры Shark и запрашиваем указатель на нее с помощью оператора &. Этот указатель привязан к переменной s. Затем мы переопределяем переменную s со значением nil с помощью оператора s = nil. Наконец, мы пытаемся вызвать метод SayHello с переменной s. Вместо получения дружественного сообщения от Sammy, мы получим панику, потому что мы пытались получить доступ к недействительному адресу в памяти. Поскольку переменная s равна nil, когда функция SayHello вызывается, она пытается получить доступ к полю Name типа *Shark. Поскольку это указатель получателя, а получатель в данном случае nil, генерируется паника, поскольку он не может разыменовывать указатель nil.

Хотя мы задали для s значение nil явно в данном примере, на практике это происходит менее явно. Когда вы увидите паники с разыменованием нулевого указателя, убедитесь, что вы настроили корректно любые ссылочные переменные, которые задали.

Паники, сгенерированные из нулевых указателей и доступа вне диапазона — это две самые часто возникающие паники, генерируемые средой выполнения. Также вы можете вручную генерировать панику с помощью встроенной функции.

**Использование встроенной функции panic**\
Также мы можем генерировать наши собственные паники с помощью встроенной функции panic. В качестве аргумента потребуется одна строка, которая будет сообщением паники, которую мы будем генерировать. Обычно это сообщение менее многословное, чем простая перезапись нашего кода для возвращения ошибки. Более того, мы можем использовать это в наших собственных пакетах, чтобы указать разработчикам, что они могли допустить ошибку при использовании кода нашего пакета. При возможности рекомендуется возвращать значения error для потребителей нашего пакета.

Запустите этот код, чтобы увидеть, как паника генерируется функцией, вызываемой другой функцией:

```
package main

func main() {
	foo()
}

func foo() {
	panic("oh no!")
}
```

Вывод полученной паники выглядит следующим образом:

```
Output
panic: oh no!

goroutine 1 [running]:
main.foo(...)
	/tmp/sandbox494710869/prog.go:8
main.main()
	/tmp/sandbox494710869/prog.go:4 +0x40
```

Здесь мы определяем функцию foo, которая вызывает встроенную функцию panic со строкой "oh no!". Эта функция вызывается нашей функцией main. Обратите внимание, что вывод содержит сообщение: panic: oh no!​​​ и трассировка стека отображает одну гоурутину с двумя строками в трассировке стека: одна для функции main() и одна для нашей функции foo().

Мы увидели, что паники вызывают прекращение работы программы в момент генерации. Это может создавать проблемы при наличии открытых ресурсов, которые необходимо надлежащим образом закрыть. Go обеспечивает механизм выполнения какого-либо кода всегда, даже при наличии паники.

**Отсроченные функции**\
Ваша программа может иметь ресурсы, которые она должна очищать надлежащим образом, даже при обработке паники средой выполнения. Go позволяет отложить исполнение вызова функции, пока вызывающая функция не завершит работу. Отсроченные функции запускаются даже при наличии паники, а также используются как механизм обеспечения защиты от хаотического характера паники. Функции откладываются при обычном вызове с префиксом для всего объявления в виде ключевого слова defer, например, defer sayHello(). Запустите этот пример, чтобы увидеть, как сообщение может быть выведено, даже если была сгенерирована паника:

```
package main

import "fmt"

func main() {
	defer func() {
		fmt.Println("hello from the deferred function!")
	}()

	panic("oh no!")
}
```

Вывод, получаемый из этого примера, будет выглядеть следующим образом:

```
Output
hello from the deferred function!
panic: oh no!

goroutine 1 [running]:
main.main()
	/Users/gopherguides/learn/src/github.com/gopherguides/learn//handle-panics/src/main.go:10 +0x55
```

Внутри функции main в данном примере мы вначале используем ключевое слово defer для вызова анонимной функции, которая выводит сообщение "hello from the deferred function!". Функция main сразу же генерирует панику с помощью функции panic. В выводе этой программы мы вначале видим, что отсроченная функция выполняется и выводит свое сообщение. После этого следует паника, которую мы генерируем в main.

Отсроченные функции обеспечивают защиту от неожиданной природы паники. Внутри отсроченных функций Go также позволяет остановить прекращение паникой работы программы Go, используя другую встроенную функцию.

{{</indent-sm>}}
29. Як відловлювати паніки?

**Обработка паник**\
Паники имеют единый механизм восстановления — встроенную функцию recover. Эта функция позволяет перехватить панику по ходу работы через стек вызовов и не допустить неожиданное прекращение работы вашей программы. Она имеет строгие правила для использования, но может быть бесценной в рабочем приложении.

Поскольку функция recover входит в пакет builtin, она может вызываться без импорта дополнительных пакетов:

```
package main

import (
	"fmt"
	"log"
)

func main() {
	divideByZero()
	fmt.Println("we survived dividing by zero!")

}

func divideByZero() {
	defer func() {
		if err := recover(); err != nil {
			log.Println("panic occurred:", err)
		}
	}()
	fmt.Println(divide(1, 0))
}

func divide(a, b int) int {
	return a / b
}
```

Результат выполнения будет выглядеть так:

```
Output
2009/11/10 23:00:00 panic occurred: runtime error: integer divide by zero
we survived dividing by zero!
```

Наша функция main в данном примере вызывает функцию, которую мы определили, divideByZero. В этой функции мы используем defer для вызова анонимной функции, которая отвечает за работу с любыми паниками, которые могут возникнуть при исполнении divideByZero. Внутри этой отсроченной анонимной функции мы вызываем функцию recover и присваиваем ошибку, которую она возвращает, для переменной. Если divideByZero генерирует панику, это значение error будет настроено, в противном случае это значение будет nil. Сравнив переменную err с nil, мы можем обнаружить наличие паники, а в данном случае мы будем регистрировать панику с помощью функции log.Println, как при любой другой ошибке.

После этой отсроченной анонимной функции мы вызываем другую функцию, которую мы определили, divide, и попытаемся вывести ее результаты с помощью fmt.Println. Предоставленные аргументы приведут к тому, что divide будет выполнять деление на ноль, что вызовет панику.

В выводе этого примера мы вначале видим сообщение журнала от анонимной функции, которая восстанавливает панику, за которым следует сообщение we survived dividing by zero!. Мы действительно сделали это благодаря встроенной функции recover, которая останавливает вызывающую катастрофические последствия панику, которая могла вызвать прекращение работы программы Go.

Значение err, возвращаемое recover(), — это то же самое значение, которое было предоставлено при вызове panic(). Поэтому особенно важно убедиться, что значение err равно nil только при отсутствии паники.

**Обнаружение паник с помощью recover**\
Функция recover опирается на значение ошибки при определении того, 
была ли сгенерирована паника или нет. Поскольку аргументом для функции panic 
служит пустой интерфейс, это может быть любой тип. Нулевое значение для любого типа, 
включая пустой интерфейс, — это nil. Необходимо избегать применения nil в качестве 
аргумента для panic, как показано в данном примере:

```
package main

import (
	"fmt"
	"log"
)

func main() {
	divideByZero()
	fmt.Println("we survived dividing by zero!")

}

func divideByZero() {
	defer func() {
		if err := recover(); err != nil {
			log.Println("panic occurred:", err)
		}
	}()
	fmt.Println(divide(1, 0))
}

func divide(a, b int) int {
	if b == 0 {
		panic(nil)
	}
	return a / b
}
```

Результат будет выглядеть так:

```
Output
we survived dividing by zero!
```

Этот пример повторяет предыдущий пример, использующий recover, с некоторыми изменениями. 
Функция divide была изменена, чтобы проверить, имеет ли разделитель b значение 0. 
Если да, она генерирует панику с помощью встроенной функции panic с аргументом nil. 
Вывод на этот раз не включает сообщение журнала, демонстрируя, что паника возникала 
даже при его создании с помощью функции divide. Именно это молчаливое поведение служит 
причиной того, что очень важно убедиться, что аргумент функции panic не равен nil.

31. Як отримати теперішній час?
{{<indent-sm>}}

**Приклад 1**
```
// Golang program to get the current time
package main
  
// Here "fmt" is formatted IO which
// is same as C’s printf and scanf.
import "fmt"
  
// importing time module
import "time"
  
// Main function
func main() {
  
    // Using time.Now() function.
    dt := time.Now()
    fmt.Println("Current date and time is: ", dt.String())
}
```
```
Current date and time is:  2009-11-10 23:00:00 +0000 UTC m=+0.000000001
```

**Приклад 2**
```

// Golang program to get the current time
package main
  
// Here "fmt" is formatted IO which
// is same as C’s printf and scanf.
import "fmt"
  
// importing time module
import "time"
  
// Main function
func main() {
  
    // Using time.Now() function.
    dt := time.Now()
    fmt.Println(dt.Format("01-02-2006 15:04:05"))
  
}
```
```
11-10-2009 23:00:00
```

{{</indent-sm>}}
32. Що таке iota?
{{<indent-sm>}}

**The Constant Generator iota**\
A const declaration may use the constant generator iota, which is used to create a sequence of related values without spelling out each one explicitly. In a const declaration, the value of iota begins at zero and increments by one for each item in the sequence.

Here’s an example from the time package, which defines named constants of type Weekday for the days of the week, starting with zero for Sunday. Types of this kind are often called enu- merations, or enums for short.
```
type Weekday int
     const (
         Sunday Weekday = iota
         Monday
         Tuesday
         Wednesday
         Thursday
         Friday
         Saturday
)
```

This declares Sunday to be 0, Monday to be 1, and so on.

We can use iota in more complex expressions too, as in this example from the net package where each of the lowest 5 bits of an unsigned integer is given a distinct name and boolean interpretation:

```
type Flags uint

const (
	FlagUp           Flags = 1 << iota // is up
	FlagBroadcast                      // supports broadcast access capability
	FlagLoopback                       // is a loopback interface
	FlagPointToPoint                   // belongs to a point-to-point link
	FlagMulticast                      // supports multicast access capability
)
```

As iota increments, each constant is assigned the value of 1 << iota, which evaluates to suc- cessive powers of two, each corresponding to a single bit. We can use these constants within functions that test, set, or clear one or more of these bits:

```
func IsUp(v Flags) bool     { return v&FlagUp == FlagUp }

func TurnDown(v *Flags)     { *v &^= FlagUp }

func SetBroadcast(v *Flags) { *v |= FlagBroadcast }

func IsCast(v Flags) bool   { return v&(FlagBroadcast|FlagMulticast) != 0 }

func main() {
	var v Flags = FlagMulticast | FlagUp
	fmt.Printf("%b %t\n", v, IsUp(v)) // "10001 true"
	TurnDown(&v)
	fmt.Printf("%b %t\n", v, IsUp(v)) // "10000 false"
	SetBroadcast(&v)
	fmt.Printf("%b %t\n", v, IsUp(v))   // "10010 false"
	fmt.Printf("%b %t\n", v, IsCast(v)) // "10010 true"
}

```

As a more complex example of iota, this declaration names the powers of 1024:

```
const (
	_   = 1 << (10 * iota)
	KiB // 1024
	MiB // 1048576
	GiB // 1073741824
	TiB // 1099511627776  (exceeds 1 << 32)
	PiB // 1125899906842624
	EiB // 1152921504606846976
	ZiB // 1180591620717411303424 (exceeds 1 << 64)
	YiB // 1208925819614629174706176
)
```

The iota mechanism has its limits. For example, it’s not possible to generate the more famil- iar powers of 1000 (KB, MB, and so on) because there is no exponentiation operator.

{{</indent-sm>}}
33. Яка різниця між слайсом і масивом?
{{<indent-sm>}}

I can tell the difference between slice and array construction since an array has a known size at compile time and slices necessarily don’t.

**Arrays**\
An array is a fixed-length sequence of zero or more elements of a particular type. Because of their fixed length, arrays are rarely used directly in Go. Slices, which can grow and shrink, are much more versatile, but to understand slices we must understand arrays first.

Individual array elements are accessed with the conventional subscript notation, where subscripts run from zero to one less than the array length. The built-in function len returns the number of elements in the array.

```
func main() {
	var a [3]int             // array of 3 integers
	fmt.Println(a[0])        // print the first element
	fmt.Println(a[len(a)-1]) // print the last element, a[2]
	
	// Print the indices and elements.
	for i, v := range a {
		fmt.Printf("%d %d\n", i, v)
	}
	// Print the elements only.
	for _, v := range a {
		fmt.Printf("%d\n", v)
	}
}
```

By default, the elements of a new array variable are initially set to the zero value for the ele- ment type, which is 0 for numbers. We can use an array literal to initialize an array with a list of values:

```
var q [3]int = [3]int{1, 2, 3}
var r [3]int = [3]int{1, 2}
fmt.Println(r[2]) // "0"
```

In an array literal, if an ellipsis ‘‘...’’ appears in place of the length, the array length is deter- mined by the number of initializers. The definition of q can be simplified to

```
q := [...]int{1, 2, 3}
fmt.Printf("%T\n", q) // "[3]int"
```

**Slices**\

Slices represent variable-length sequences whose elements all have the same type. 
A slice type is written []T, where the elements have type T; it looks like an array type without 
a size.

Arrays and slices are intimately connected. A slice is a light lightweight data structure 
that gives access to a subsequence (or perhaps all) of the elements of an array, 
which is known as the slice’s underlying array. A slice has thre e components: a pointer, 
a length, and a capacity. The pointer points to the first element of the array that is 
reachable through the slice, which is not necessarily the array’s first element. 
The length is the number of slice elements; it can’t exceed the capacity, which 
is usually the number of elements between the start of the slice and the end of the underlying 
array. The built-in functions len and cap return those values.

The slice is Go’s most important data structure and it’s represented as a three word data structure.

![Slice](/images/go/fig3-5.png)

Constructing a slice can be done in several ways.

```
// Slice of string set to its zero value state.
var slice []string

// Slice of string set to its empty state.
slice := []string{}

// Slice of string set with a length and capacity of 5.
slice := make([]string, 5)

// Slice of string set with a length of 5 and capacity of 8.
slice := make([]string, 5, 8)

// Slice of string set with values with a length and capacity of 5.
slice := []string{"A", "B", "C", "D", "E"}
```

Multiple slices can share the same underlying array and may refer to overlapping parts of 
that array. Figure 4.1 shows an array of strings for the months of the year, and two overlapping 
slices of it.

![Slice](/images/go/fig-k4-1.png)

{{</indent-sm>}}

35. З яких частин складається змінна типу slice?
{{<indent-sm>}}

The slice is Go’s most important data structure and it’s represented as a three word data structure.
![Slice](/images/go/fig3-5.png)

The length of a slice represents the number of elements that can be read and written to. The capacity represents the total number of elements that exist in the backing array from that pointer position.

{{</indent-sm>}}
36. Як працює append?
{{<indent-sm>}}

**Appending With Slices**\
The language provides a built-in function called append to add values to an existing slice.

```
var data []string
for record := 1; record <= 102400; record++ {
    data = append(data, fmt.Sprintf("Rec: %d", record))
}
```

The append function works with a slice even when the slice is initialized to its zero value state. The API design of append is what’s interesting because it uses value semantic mutation. Append gets its own copy of a slice value, it mutates its own copy, then it returns a copy back to the caller.

Why is the API designed this way? This is because the idiom is to use value semantics to move a slice value around a program. This must still be respected even with a mutation operation. Plus, value semantic mutation is the safest way to
perform mutation since the mutation is being performed on the function’s own copy of the data in isolation.

Append always maintains a contiguous block of memory for the slice’s backing array, 
even after growth. This is important for the hardware.

![Slice](/images/go/fig3-6.png)

Every time the append function is called, the function checks if the length and capacity 
of the slice is the same or not. If it’s the same, it means there is no more room in the 
backing array for the new value. In this case, append creates a new backing array 
(doubling or growing by 25%) and then copies the values from the old array into the new one. 
Then the new value can be appended.

![Slice](/images/go/fig3-7.png)

If it’s not the same, it means that there is an extra element of capacity existing 
for the append. An element is taken from capacity and added to the length of the slice. 
This makes an append operation very efficient.

When the backing array has 1024 elements of capacity or less, new backing arrays are 
constructed by doubling the size of the existing array. 
Once the backing array grows past 1024 elements, growth happens at 25%.

{{</indent-sm>}}

38. Що таке len і capacity в slice?
{{<indent-sm>}}

**Slice Length vs Capacity**
The length of a slice represents the number of elements that can be read and written to. The capacity represents the total number of elements that exist in the backing array from that pointer position.

Because of syntactic sugar, slices look and feel like an array.

```
slice := make([]string, 5)
slice[0] = "Apple"
slice[1] = "Orange"
slice[2] = "Banana"
slice[3] = "Grape"
slice[4] = "Plum"
```

I can tell the difference between slice and array construction since an array has a known size at compile time and slices necessarily don’t.

If I try to access an element beyond the slice’s length, I will get a runtime error.

```
slice := make([]string, 5)
slice[5] = "Raspberry"
Compiler Error:
Error: panic: runtime error: index out of range slice[5] = "Runtime error"
```

In this example, the length of the slice is 5 and I’m attempting to access the 6th element, which does not exist.

{{</indent-sm>}}

39. Що таке пакети?
{{<indent-sm>}}

Packages in Go serve the same purposes as libraries or modules in other languages, 
supporting modularity, encapsulation, separate compilation, and reuse. 
The source code for a package resides in one or more .go files, usually in a directory 
whose name ends with the import path; for instance, the files of the gopl.io/ch1/helloworld 
package are stored in directory $GOPATH/src/gopl.io/ch1/helloworld.

Each package serves as a separate name space for its declarations. Within the image package, for example, 
the identifier Decode refers to a different function than does the same identifier in the 
unicode/utf16 package. To refer to a function from outside its package, we must qualify the 
identifier to make explicit whether we mean image.Decode or utf16.Decode.

Packages also let us hide information by controlling which names are visible 
outside the pack- age, or exported. In Go, a simple rule governs which identifiers 
are exported and which are not: exported identifiers start with an upper-case letter.

{{</indent-sm>}}
40. Що таке інтерфейси і як вони працюють?
{{<indent-sm>}}

Interface types express generalizations or abstractions about the behaviors of other types. 
By generalizing, interfaces let us write functions that are more flexible and adaptable 
because they are not tied to the details of one particular implementation.

Many object-oriented languages have some notion of interfaces, but what makes 
Go’s interfaces so distinctive is that they are satisfied implicitly. 
In other words, there’s no need to declare all the interfaces that a given 
concrete type satisfies; simply possessing the necessary methods is enough. 
This design lets you create new interfaces that are satisfied by existing concrete 
types without changing the existing types, which is particularly useful for types 
defined in packages that you don’t control.

All the types we’ve looked at so far have been concrete types. 
A concrete type specifies the exact representation of its values and exposes 
the intrinsic operations of that representation, such as arithmetic for numbers, 
or indexing, append, and range for slices. A concrete type may also provide additional 
behaviors through its methods. When you have a value of a con- crete type, you know 
exactly what it is and what you can do with it.

There is another kind of type in Go called an interface type. An interface is an abstract type. It doesn’t expose the representation or internal structure of its values, or the set of basic
operations they support; it reveals only some of their methods. When you have a value of an interface type, you know nothing about what it is; you know only what it can do, or more precisely, what behaviors are provided by its methods.


An interface type specifies a set of methods that a concrete type must possess to be considered an instance of that interface.


{{</indent-sm>}}
41. Для чого потрібні інтерфейси в Golang?
42. Що таке тип даних string?
43. Чим відрізняються лапки в Go (подвійні, одинарні, зворотні): "",'', ``?
44. Що таке rune?
45. Чи може змінна типу string приймати nil-значення?
46. Чи можна повернути з функції кілька значень?
{{<indent-sm>}}

Так, можна

{{</indent-sm>}}
47. Що відбувається під час конкатенації рядків?
48. Як ефективно склеїти кілька рядків?
49. Як записати в файл?
50. Що таке структура?
51. Що буде, якщо викликати log.Fatal?
52. Поясніть різницю між конкурентністю і паралельністю?
53. Як оголосити відкладений виклик?
54. Що таке канал? Які типи каналів ви знаєте? Для чого вони потрібні?
55. Яка різниця між буферизованим і небуферизованим каналами?
56. Що буде, якщо читати із закритого каналу? Що буде, якщо писати у закритий канал?
57. Як перевірити, що змінна типу map має збережене значення для певного ключа?
58. Які стандартні env-змінні в Go?
59. Яка різниця між value & pointer receiver?
60. Як зробити type assertion?
61. Як та навіщо робити type assertion?
62. Як написати benchmark?
63. В якому порядку виконуються кейси в select?
{{<indent-sm>}}

Для роботи з декількома каналами існує select:

```go
select {
case x := <-ch1:
	// ...
case ch2 <- y:
	// ...
default:
    // ...
}
```

коли код доходить до select, то він чекає коли можна щось отримати з ch1 або
коли можна щось відправити в ch2, і коли щось з них з"явиться, той case і виконається.
Якщо можна і читати з ch1 і писати в ch2, то гілка case виконається рандомно, або на ch1 або на ch2.
Коли ввели default, то коли жоден з каналів не готовий, то буде виконаний default.

{{</indent-sm>}}

#### Практичні завдання
58. Реалізувати алгоритм двійкового пошуку елемента у слайсі.
59. Є код. Що виведеться на екран? Що потрібно зробити, щоб побачити запропонований висновок Foo1 і Foo2?
    ```go
    package main
    
    import (
       "fmt"
    )
    
    func main() {
       fmt.Println("start")
       c := make(chan int)
       go Foo1(c)
       go Foo2(c)
       fmt.Println("end")
    }
    
    func Foo1(c chan int) {
       fmt.Println("foo1 begin")
       c <- 1
       fmt.Println("foo1 end")
    }
    
    func Foo2(c chan int) {
       fmt.Println("foo2 begin")
       <-c
       fmt.Println("foo2 end")
    }
    
    ```
   
60. Є код. Що виведеться на екран? Як вивести на екран літери?
    ```go
    package main

    import (
       "fmt"
    )
    
    func main() {
       arr := []string{"a", "b", "c"}

       for x := range arr {
           fmt.Println(x)
       }
    }
    
    ```
61. Поміняйте місцями значення двох змінних без тимчасової допоміжної змінної.
{{<indent-sm>}}

```
package main

import "fmt"

func main() {
	a := 1
	b := 2

	swapInPlace(&a, &b)
	fmt.Printf("a=%#v, b=%#v \n", a, b)

	a, b = swap(a, b)
	fmt.Printf("a=%#v, b=%#v", a, b)
}

func swap(a int, b int) (int, int) {
	a, b = b, a

	return a, b
}

func swapInPlace(a *int, b *int) {
	*a, *b = *b, *a
}

func main() {
	a := 1
	b := 2

	swapInPlace(&a, &b)
	fmt.Printf("a=%#v, b=%#v", a, b)
}
```

```
a=2, b=1 
a=1, b=2
```

{{</indent-sm>}}
62. Оберніть slice у зворотному порядку.
{{<indent-sm>}}

```
// reverse reverses a slice of ints in place.
func reverse(s []int) {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
}
```

```
a := [...]int{0, 1, 2, 3, 4, 5}
reverse(a[:])
fmt.Println(a) // "[5 4 3 2 1 0]"
```

{{</indent-sm>}}
63. Перемістіть усі zero values у кінець масиву.

#### Middle
#### Software engineering
1. У чому переваги та недоліки використання protobuf у порівнянні з JSON?
2. Назвіть деякі з принципів 12-factor-app. Для чого використовують graceful shutdown?
3. Що таке dependency injection? А dependency inversion?
4. У вас є ssh-доступ на Linux-сервер, де запущено вебсервер. З сервером періодично відбуваються незаплановані рестарти. Сервер записує логи у файли (розмір лог-файлу > 10 Mb). Які *nix-команди ви можете використати, щоб проаналізувати проблему?
5. Як працює TLS handshake?
6. Яка різниця між TCP & UDP? В якій ситуації UPD краще?
7. Розкажіть у найдрібніших деталях, що відбувається, коли клієнт посилає запит на сервер, а сервер цей запит отримує.
8. Що таке Clean Architecture? Наведіть приклад.
9. Що таке оперативна пам’ять?
10. Як різниця між stack & heap?
11. Для чого потрібні Docker та Kubernetes?
12. Розкажіть про абстракції Kubernetes, з якими працювали?
13. Що таке Pod? Як він влаштований?
14. Розкажіть про Data structures: stack, queue, linked list, trie, balanced tree.

#### Бази даних
15. Які шляхи пошуку повільних SELECT-запитів у RDBMS? Які способи пришвидшення таких запитів?
16. Що таке нормальні форми бази даних?
17. Що таке індекси? Які їхні недоліки?
18. Які структури даних можуть використовуватися в індексах баз даних? Як вони працюють?
19. Яка різниця між foreign & primary key?
20. Поняття міграції у контексті баз даних. У чому переваги підходу з міграціями?
21. Що таке реплікація даних? Які варіанти реплікації існують для баз, з якими ви працювали?
22. Яка різниця між SQL і NoSQL?
23. Які типи NoSQL баз даних ви знаєте? Наведіть приклади. Яка між ними різниця?
24. Що таке колонкова БД? Переваги та недоліки.
25. Що таке документоорієнтована БД? Переваги та недоліки.
26. Розкажіть про роботу з key-value базами даних (бажано з власного досвіду).
27. З якими типами даних Redis у вас є практичний досвід? Назвіть приклади, коли їх доцільно використовувати.


#### Go
28. Якими бібліотеками Go ви користувалися для доступу до RDBMS? Які у них позитивні та негативні сторони?
29. Для чого використовують Context? Які є варіанти скасовування контекстів?
30. Якими способами можна виключити (приховати) поля структури при JSON-серіалізації?
31. Назвіть примітиви пакету sync стандартної бібліотеки. Яке призначення та приклади застосування sync.WaitGroup?
32. Яка різниця між Mutex та RWMutex?
33. Які є способи зупинити N горутин, запущених одночасно (наприклад, worker pool)?
34. Що таке замикання функцій?
35. Поясніть різницю між switch і select?
36. Які є способи дістати дані з JSON?
37. Як у Go реалізовані конструкції циклів?
38. Як влаштований тип map?
39. Який порядок перебору map?
40. Що таке серіалізація? Де вона застосовується?
41. Чи можна використовувати nil для ініціалізації змінної?
42. Чи можна задати місткість map? Чи можна отримати місткість map?
43. Як дізнатися кількість символів у рядку?
44. Що таке кодогенерація і для чого вона потрібна?
45. Чим відрізняється goroutine від OS thread?
    {{<indent-sm>}}

    **Goroutine**: A Goroutine is a function or method which executes independently 
    and simultaneously in connection with any other Goroutines present 
    in your program. Or in other words, every concurrently executing 
    activity in Go language is known as a Goroutines.
 
    **Thread**: A lightweight process is a part of an operating system which is responsible for executing 
    an application. Every program that executes on your system is a process and to run the code inside 
    the application a process uses a term known as a thread. A thread is a lightweight process, 
    or in other words, a thread is a unit which executes the code under the program. 
    So every program has logic and a thread is responsible for executing this logic. Operating system manages the thread.

    [Что такое горутины и каков их размер?](https://habr.com/ru/company/otus/blog/527748/)

    {{</indent-sm>}}
48. Як і для чого використовують io.Reader і io.Writer?
49. Як перетворити []io.ReadWriter на []io.Reader?
50. Як вказати головній горутині очікувати завершення роботи всіх робочих горутин?
51. Чи завжди буде швидше передача Pointer як аргументу функції?
52. Що таке варіативна змінна функції? Як працювати з цією змінною?
53. Як працювати з пакетом internal?
54. Як працює імпорт через крапочку і чому це погана практика?
55. Як працює імпорт через підкреслення?
56. Що таке defer()?
57. Як працювати з goto?
58. Що таке reflect.DeepEqual() і reflect.TypeOf()?
59. Як розпарсити час?
60. Як порівняти дві дати?
61. Що таке вказівник і як з ним працювати?
62. Як перевірити, чи змінна імплементує інтерфейс?
63. Що таке embedding?
64. Опишіть кроки процесу тестування.
65. Як писати тести? Що таке табличні тести?
66. Що таке memory leak? Які є способи його виявлення? Як його позбутися?
67. Що таке race condition? Які є способи його виявлення? Як його позбутися?

#### Практичні завдання
67. Реалізувати перевірку на слова на анаграму. Написати тест і бенчмарк. Оцінити складність розробленого алгоритму.
68. Є код. Що виведеться на екран? Чому?
    ```go
    package main
    
    import (
        "fmt"
    )
    
    func main() {
        a := []int{}
        a = append(a, 1)
        a = append(a, 2)
        a = append(a, 3)
        Add(a)
        fmt.Println(a)
    }
    
    func Add(a []int) {
        a = append(a, 4)
    }
    ```
69. Є код. Чи можна передбачити, що виведеться на екран? Чому?
    ```go
    package main
    
    import (
        "fmt"
        "sync"
    )
    
    var c = make(chan *int, 3)
    var data = []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
    
    func main() {
        var wg sync.WaitGroup
        wg.Add(2)
    
        go save(&wg)
        go read(&wg)
    
        wg.Wait()
    }
    
    func save(wg *sync.WaitGroup) {
        defer wg.Done()
        for _, val := range data {
            c <- &val
        }
    }
    
    func read(wg *sync.WaitGroup) {
        defer wg.Done()
        for i := 0; i < len(data); i++ {
            val := <-c
            fmt.Println("read:", *val)
        }
    }
    ```
70. Реалізуйте Stack (LIFO).
71. Реалізуйте linked list.
72. Задача про суму підмножини (Subset Sum Problem). Дано: множина позитивних цілих чисел і значення sum. Визначте, чи існує підмножина даної множини з сумою, яка дорівнює значенню sum.
    ```
    Input: set[] = {3, 34, 4, 12, 5, 2}, sum = 9
    
    Output: True
    ```

#### Senior
#### Software engineering
1. Що таке процес і потік? Як вони пов’язані між собою? Чи мають різні процеси чи потоки доступу до однієї області пам’яті?
2. Які інструменти зазвичай використовують для збору метрик і логів? Як працює Prometheus?
3. Як працює docker під капотом?
4. Як працює load balancer під капотом?
5. Для чого потрібні черги?
6. Що таке CQRS?
7. Які архітектури програмного забезпечення ви знаєте?
8. Яка різниця між мікросервісами та монолітом? Які є переваги та недоліки?
9. Як побудувати міжсервісну транзакцію?
10. Що таке розподілені транзакції? Як реалізувати?
11. Які проблеми вирішує патерн Saga?
12. Як реалізувати аутентифікацію в мікросервісній архітектурі?
13. Що таке Event Sourcing?
14. Сформулюйте CAP-теорему.
15. Розкажіть про Raft Consensus Algorithm.
16. У чому різниця між імперативною і декларативною парадигмою програмування? Наведіть приклади мов.


#### Бази даних
17. Для чого потрібні графові бази даних?
18. Якщо точкові дані прив’язані до часу, які бази даних варто використовувати для збереження цих даних?
19. Що таке Materialized View? У чому відмінність від звичайного View?
20. Що таке ACID? Прокоментуйте, як ACID реалізований в PostgreSQL.
21. Яка різниця між BASE та ACID?
22. Назвіть рівні ізоляції транзакцій?
23. Досвід оптимізації бази. Які інструменти використовували?
24. Що таке шардинг? Які види є?
25. Як працює master-slave реплікація?
26. Як працюють індекси? Як вибрати індекси в таблицях?
27. Розкажіть про optimistic та pessimistic locking.


#### Go
28. За що відповідає змінна GOMAXPROCS? Яке її значення за замовчуванням?
29. Хто відповідає за планування горутин? Розкажіть про алгоритм роботи планувальника. Навіщо він потрібен, якщо вже й так є системні потоки?
30. Розкажіть про алгоритм роботи garbage collector. Mark and sweep, Reference counting та оптимізації алгоритму в мові Go. Що таке stop the world?
31. Які види багатозадачності ви знаєте? Який з них використовують у Go?
32. Для чого потрібна рефлексія? У чому різниця між рефлексією та кодогенерацією?
33. У чому різниця між value та reference типом? Назвіть декілька прикладів у мові Go.
34. Як зупинити горутину?
35. Як в Go реалізується спадкування?
36. Що таке lvalue і rvalue?
37. Яке у slice zero value?
38. Як вбудувати стандартний профайлер у свій застосунок?
39. Що таке map-reduce? Як його реалізувати в Go?
40. Як в Go працює префіксний інкремент/декремент?
41. Що буде, якщо перетворити в JSON-об’єкт, структура якого містить поля з малими літерами в назвах?
42. Як перервати for/switch або for/select?
43. Як можна оптимізувати виконання великої кількості послідовних операцій читання або запису?
44. Чи можна викликати метод у вказівника (pointer) на структуру, якщо він дорівнює nil?
45. Що буде при спробі запису в закритий канал?
46. Що таке взаємне блокування (deadlock)?
47. У чому особливість nil-каналів?
48. У яких випадках варто використовувати м’ютекси, а не канали, та навпаки?
49. Що таке білд-теги?
50. Як реалізувати LRU cache?
51. Що таке SSA-представлення?
52. Що ви знаєте про роботу з плагінами на Go?
53. Що таке аліаси типів?
54. Що таке падінги в структурах і на що вони впливають?
55. Що таке escape-аналіз?
56. Яка різниця між стеком і купою?
57. Що нам дає пакет unsafe?
58. Чи можна змінити певний символ у рядку? А за допомогою пакета unsafe?
59. Як працювати з рефлексією і що ми можемо з нею зробити?
60. Як під капотом виглядають слайси й мапи?
61. Як працювати з copy()?
62. Як працювати з sync.Pool і sync.Map? Які підводні камені вони мають?
63. Розкажіть про канкаренсі-патерни в Go.
64. Як у Go реалізована арифметика вказівників?*

#### Практичні завдання
65. Побудувати архітектуру бекенду для оренди самокатів типу Kiwi чи Bolt. Які мікросервіси будуть? Які мови та інструменти? Який клауд-провайдер? Що варто використати як інфраструктуру? Які бази даних та шини повідомлень?
66. Уявімо, що ви провідний розробник на проєкті, як би ви розподілили ролі в команді із 5 людей для найефективнішої роботи?
67. Є код. Чому err! = nil?
68. Вам дають ssh на сервер і просять полагодити вебзастосунок, який «впав». Інформації про сервер і сервіси немає. Які команди ви будете використовувати, щоб зібрати інформацію, знайти та усунути проблему?
69. Реалізуйте двійкову структуру даних дерева пошуку в Go.
70. Знайдіть максимальну суму шляху в трикутнику. (Подано числа у формі трикутника, починаючи з верхньої частини трикутника і рухаючись до сусідніх чисел у рядку внизу, знаходимо максимальну загальну кількість зверху вниз.)
