---
title: Algorithms
subtitle: Алгоритми
date: 2022-06-19
tags: ["algorithms", "markdown"]
---

### JS Common

1. Масиви 
   {{<indent-sm>}}
   **How Many Numbers Are Smaller Than the Current Number**
   https://leetcode.com/problems/how-many-numbers-are-smaller-than-the-current-number/
   ```
   Given the array nums, for each nums[i] find out how many numbers 
   in the array are smaller than it. That is, 
   for each nums[i] you have to count the number of valid j's 
   such that j != i and nums[j] < nums[i].
   
   Return the answer in an array.
   
   Example 1:
   Input: nums = [8,1,2,2,3]
   Output: [4,0,1,1,3]
   Explanation: 
   For nums[0]=8 there exist four smaller numbers than it (1, 2, 2 and 3). 
   For nums[1]=1 does not exist any smaller number than it.
   For nums[2]=2 there exist one smaller number than it (1). 
   For nums[3]=2 there exist one smaller number than it (1). 
   For nums[4]=3 there exist three smaller numbers than it (1, 2 and 2).
   
   Example 2:
   Input: nums = [6,5,4,8]
   Output: [2,1,0,3]
   
   Example 3:
   Input: nums = [7,7,7,7]
   Output: [0,0,0,0]
    
   Constraints:
   2 <= nums.length <= 500
   0 <= nums[i] <= 100
   ```
   {{</indent-sm>}}
   {{<toggle>}}
   
   ```javascript
   // 1
   let smallerNumbersThanCurrent = function(nums) {
       let ans = new Array(nums.length);
       let count = 0;
       
       for (let i = 0; i < nums.length; i++) {
           let a = nums[i];
           
           for (let j = 0; j < nums.length; j++) {
               let b = nums[j];
               if(i != j && a > b) {
                   count++;
               }
           }
           
           ans[i] = count;
           count = 0;
       }
       
       return ans;
   };
   ```
   
   ```javascript
   function solution(nums) {
     let count = new Array(101).fill(0);
     let ans = new Array(nums.length).fill(0);
   
     for (let i = 0; i < nums.length; i++) {
       count[nums[i]]++;
     }
   
     for (let i = 1; i < 100; i++) {
       count[i] += count[i - 1];
     }
   
     for (let i = 0; i < nums.length; i++) {
       if (nums[i] === 0) {
         ans[i] = 0;
       } else {
         ans[i] = count[nums[i] - 1];
       }
     }
   
     return ans;
   }
   ```

   {{</toggle>}}


   {{<indent-sm>}}

   ---------------

   **1)**
   ```
   Дан массив чисел nums. 
   Running sum для элемента i задан как сумма элементов [0..i], 
   то есть runningSum[i] = sum(nums[0]..nums[i]).
   Вычислите и верните массив running sum.
   
   Пример:
   Input: nums = [1, 2, 3]
   Output: [1, 3, 6]
   
   ```
   
   {{</indent-sm>}}
   
   {{<toggle>}}
   
   ```javascript
   //Time: O(n), Space: O(0)
   const solution = (nums) => {
       for (let i = 1; i < nums.length; i++) {
           nums[i] = nums[i] + nums[i - 1];
       }
       return nums
   }
   ```

   {{</toggle>}}

   ---------------

   {{<indent-sm>}}

   **2)**

   ```
   Вам дана матрица MxN целых чисел - accounts. Где accounts[i][j] хранит количество денег
   ith клиент имеет на jth счету. Верните состояние самого богатого клиента.
   
   Состояние клиента это сумма всех его средств на всех счетах.
   
   Пример:
   Input: accounts = [[1,5],[7,3],[3,5]]
   Output: 10
   ```
   
   {{</indent-sm>}}

   {{<toggle>}}
   
   ```javascript
   //Time: O(n*m), Space: O(1)
   const solution = (accounts) => {
       let sum = Number.MIN_SAFE_INTEGER;
   
       for (let i = 0; i < accounts.length; i++) {
           let currentClientMoney = 0;
           for (let j = 0; j < accounts[i].length; j++) {
               currentClientMoney += accounts[i][j];
           }
   
           if (currentClientMoney > sum) {
               sum = currentClientMoney;
           }
       }
   
       return sum;
   }
   ```
   
   {{</toggle>}}

   ---------------

   {{<indent-sm>}}

   **3)**
   ```
   Вам дан массив целых чисел nums.
   Верните минимальное число операций требуемых для того, 
   что бы сделать nums строго возрастающим.
   
   Одна операция - это увеличение любого элемента в массиве на 1.
   
   Массив nums будет строго возрастающим если nums[i] < nums[i+1] 
   для всех 0 <= i < nums.length - 1
   Массив с одним элементом считается строго возрастающим.
   
   Пример:
   Input: nums = [1,1,1]
   Output: 3
   
   Пояснение: строго возрастающим будет массив [1,2,3], потому 
   нужно увеличить nums[1] ровно 1 раз, а nums[2] 2 раза.
   ```
   
   {{</indent-sm>}}

   {{<toggle>}}
   
   ```javascript
   //Time: O(n), Space: O(1)
   const solution = (nums) => {
       let operations = 0;
       for (let i = 1; i <= nums.length; i++) {
           if (nums[i - 1] >= nums[i]) {
               operations += nums[i - 1] - nums[i] + 1;
               nums[i] += nums[i - 1] - nums[i] + 1;
           }
       }
   
       return operations
   }
   ```
   
   {{</toggle>}}

   ---------------

   {{<indent-sm>}}

   **4)**
   ```
   Вам дана матрица MxN. Верните true если это Toeplitz матрица. 
   Иначе, верните false.
   
   Toeplitz матрица это матрица в котороый каждая диагональ 
   (с верхнего левого элемента по нижний правый элемент) 
   имеет одинаковые значения.
   
   Пример Toeplitz матрицы:
   1,2,3,4
   5,1,2,3
   9,5,1,2
   Диагоналями являются: "[9]", "[5, 5]", "[1, 1, 1]", "[2, 2, 2]", "[3, 3]", "[4]".
   ```

   {{</indent-sm>}}

   {{<toggle>}}

   ```javascript
   // Time: O((n)*(m)), Space: O(0)
   const solution = (matrix) => {
       for (let i = 0; i < matrix.length - 1; i++) {
           for (let j = 0; j < matrix[i].length - 1; j++) {
               if (matrix[i][j] !== matrix[i + 1][j + 1]) {
                   return false;
               }
           }
       }
   
       return true;
   }
   ```

   {{</toggle>}}

   ---------------

   {{<indent-sm>}}

   **5)**
   ```
   Вам дан список (массив) слов words, и два слова word1, word2.
   Посчитайте кратчайшую дистанцию между этими двумя словами в массиве.
   
   word1 и word2 не совпадают и оба присутсвуют в массиве.
   
   Пример:
   Input: words = ["practice", "makes", "perfect", "coding", "makes"], 
   word1 = "coding", word2 = "practice"
   Output: 3
   ```

   {{</indent-sm>}}

   {{<toggle>}}

   ```javascript
   //Time: O(n), Space: O(1)
   const solution = (words, word1, word2) => {
       let word1Idx = -1;
       let word2Idx = -1;
       let distance = 0;
   
       for (let i = 0; i < words.length; i++) {
           if (words[i] === word1) {
               word1Idx = i;
           } else if (words[i] === word2) {
               word2Idx = i;
           }
   
           if (word1Idx > -1 && word2Idx > -1 && (distance > Math.abs(word2Idx - word1Idx) || distance === 0)) {
               distance = Math.abs(word2Idx - word1Idx);
           }
       }
   
       return distance;
   }
   ```

   {{</toggle>}}

   ---------------

2. Зв"язний список, стек, черга
   
   **Single LinkedList**
   {{<toggle>}}
   ```javascript
   // Single LinkedList 
   class Node {
       constructor(val) {
           this.val = val;
           this.next = null;
       }
   }
   
   class SinglyLinkedList {
       constructor() {
           this.head = null;
           this.tail = null;
       }
   
       // O(1)
       add(val) {
           const node = new Node(val);
   
           if (this.head == null) {
               this.head = node;
           } else {
               this.tail.next = node;
           }
           this.tail = node;
   
           return this;
       }
   
       printList() {
           let curr = this.head;
           process.stdout.write(`${curr.val}`);
           curr = curr.next;
           while (curr != null) {
               process.stdout.write(` -> ${curr.val}`);
               curr = curr.next;
           }
           process.stdout.write("\n");
           return this;
       }
   }
   
   (() => {
       new SinglyLinkedList()
           .add(1)
           .add(2)
           .add(3)
           .printList()
           .add(5)
           .add(1)
           .printList();
   })();
   ```
   
   {{</toggle>}}

   ---------------

   **Double LinkedList**   

   {{<toggle>}}
   
   ```javascript
      // Double LinkedList 
      class Node {
          constructor(val) {
              this.val = val;
              this.next = null;
              this.prev = null;
          }
      }
      
      class DoublyLinkedList {
          constructor() {
              this.head = null;
              this.tail = null;
          }
      
          add(val) {
              const node = new Node(val);
              if (this.head == null) {
                  this.head = node;
              } else {
                  this.tail.next = node;
                  node.prev = this.tail;
              }
              this.tail = node;
      
              return this;
          }
      
          // O(n)
          // basic impl, won't work for empty list
          addByIndex(idx, val) {
              let curr = this.head;
              let currIdx = 0;
              while (curr != null && currIdx != idx) {
                  curr = curr.next;
                  currIdx++;
              }
      
              if (curr == null) {
                  throw new Error("Index out of bounds");
              }
      
              const newNode = new Node(val);
              let prev = curr.prev;
              newNode.next = curr;
              newNode.prev = prev;
      
              if (prev != null) {
                  prev.next = newNode;
              }
              curr.prev = newNode;
      
              if (curr == this.head) {
                  this.head = newNode;
              }
      
              return this;
          }
      
          // O(n)
          remove(idx) {
              let curr = this.head;
              let currIdx = 0;
              while (curr != null && currIdx != idx) {
                  curr = curr.next;
                  currIdx++;
              }
      
              if (curr == null) {
                  throw new Error("Index out of bounds");
              }
      
              if (curr == this.tail) {
                  return this.removeLast();
              }
              if (curr == this.head) {
                  return this.removeFirst();
              }
      
              const prev = curr.prev;
              const next = curr.next;
      
              if (prev != null) {
                  prev.next = next;
              }
              if (next != null) {
                  next.prev = prev;
              }
      
              return this;
          }
      
          // O(1)
          removeLast() {
              if (this.head == this.tail) {
                  this.head = this.tail = null;
              } else {
                  this.tail = this.tail.prev;
                  this.tail.next = null;
              }
              return this;
          }
      
          // O(1)
          removeFirst() {
              if (this.head == this.tail) {
                  this.head = this.tail = null;
              } else {
                  this.head = this.head.next;
                  this.head.prev = null;
              }
              return this;
          }
      
          printList() {
              if (this.head == null) {
                  process.stdout.write("<Empty>\n");
                  return this;
              }
      
              let curr = this.head;
              process.stdout.write(`${curr.val}`);
              curr = curr.next;
              while (curr != null) {
                  process.stdout.write(` <-> ${curr.val}`);
                  curr = curr.next;
              }
              process.stdout.write("\n");
              return this;
          }
      }
      
      (() => {
          new DoublyLinkedList()
              .add(1)
              .add(2)
              .add(3)
              .add(5)
              .printList()
              .remove(1)
              .printList()
              .remove(2)
              .printList()
              .remove(1)
              .remove(0)
              .printList()
              .add(7)
              .add(9)
              .removeLast()
              .printList()
              .addByIndex(0, 10)
              .addByIndex(0, 11)
              .printList()
              .removeFirst()
              .printList();
      })();
      ```
   
   {{</toggle>}}
      
   ---------------

   {{<indent-sm>}}

   **Valid Parentheses**

   https://leetcode.com/problems/valid-parentheses/

   ```
   Given a string s containing just the characters 
   '(', ')', '{', '}', '[' and ']', 
   determine if the input string is valid.
   
   An input string is valid if:
   
   Open brackets must be closed by the same type of brackets.
   Open brackets must be closed in the correct order.
   
   Example 1:
   
   Input: s = "()"
   Output: true
   Example 2:
   
   Input: s = "()[]{}"
   Output: true
   Example 3:
   
   Input: s = "(]"
   Output: false
    
   
   Constraints:
   
   1 <= s.length <= 104
   s consists of parentheses only '()[]{}'.
   ```
   {{</indent-sm>}}

   {{<toggle>}}


   ```javascript
      function solution(str) {
        const stack = [];
      
        for (const char of str) {
          if (char === "(") {
            stack.push(")");
          } else if (char === "[") {
            stack.push("]");
          } else if (char === "{") {
            stack.push("}");
          } else if (stack.length === 0 || stack.pop() !== char) {
            return false;
          }
        }
      
        return stack.length === 0;
      }
   ```
   
   {{</toggle>}}

   ---------------

   {{<indent-sm>}}
   
   **Find All Duplicates in an Array**

   https://leetcode.com/problems/find-all-duplicates-in-an-array/

   ```
   Given an integer array nums of length n where all the integers of 
   nums are in the range [1, n] and each integer appears once or 
   twice, return an array of all the integers that appears twice.
   
   You must write an algorithm that runs in O(n) time and uses 
   only constant extra space.
   
   
   Example 1:
   
   Input: nums = [4,3,2,7,8,2,3,1]
   Output: [2,3]
   Example 2:
   
   Input: nums = [1,1,2]
   Output: [1]
   Example 3:
   
   Input: nums = [1]
   Output: []
   
   Constraints:
   
   n == nums.length
   1 <= n <= 105
   1 <= nums[i] <= n
   Each element in nums appears once or twice.
   ```
   
   {{</indent-sm>}}

   {{<toggle>}}
   
   ```javascript
   let findDuplicates = function(nums) {
     const res = [];
     const n = nums.length;
   
      // [4,3,2,7,8,2,3,1]
      //  0,1,2,3,4,5,6,7
      // [4,3,2,-7,8,2,3,1] position=nums[0] - 1 = 3
      // [4,3,-2,-7,8,2,3,1] position=nums[1] - 1 = 2
      // [4,-3,-2,-7,8,2,3,1] position=nums[2] - 1 = 1
      // [4,-3,-2,-7,8,2,-3,1] position=nums[3] - 1 = 6
      // ....
      // [4,-3,-2,-7,8,2,-3,1] position=Math.abs(nums[6]) - 1 = 2
     for (let i = 0; i < n; i++) {
       let position = Math.abs(nums[i]) - 1;
       if (nums[position] < 0) {
         res.push(Math.abs(nums[i]));
       } else {
         nums[position] *= -1;
       }
     }
   
     return res;
   };
   ```
   
   {{</toggle>}}

   ---------------

3. Алгоритми по пошуку та сортуванню.

   **Single Element in a Sorted Array**

   https://leetcode.com/problems/single-element-in-a-sorted-array/

   {{<indent-sm>}}
   
   ```
   You are given a sorted array consisting of only integers 
   where every element appears exactly twice, except for 
   one element which appears exactly once.
   Return the single element that appears only once.
   Your solution must run in O(log n) time and O(1) space.
   
   Example 1:
   
   Input: nums = [1,1,2,3,3,4,4,8,8]
   Output: 2
   Example 2:
   
   Input: nums = [3,3,7,7,10,11,11]
   Output: 10
   
   Constraints:
   
   1 <= nums.length <= 105
   0 <= nums[i] <= 105
   
    0 1 2 3 4 5 6 7 8
   [1,1,2,3,3,4,4,8,8]
   ```
   
   {{</indent-sm>}}
   
   {{<toggle>}}
   ```javascript
   function solution(arr) {
       let left = 0;
       let right = arr.length - 1;
   
       while (left < right) {
           let mid = left + (right - left) / 2;
   
           if (
               (mid % 2 === 0 && arr[mid] === arr[mid + 1]) ||
               (mid % 2 === 1 && arr[mid] === arr[mid - 1])
           ) {
               left = mid + 1;
           } else {
               right = mid;
           }
       }
       return arr[left];
   }
   ```
   {{</toggle>}}

   **Алгоритми пошуку**
   {{<toggle title="Лінійний пошук:">}}
 
   ```javascript
   const linearSearch = (arr, elementToSearch) => {
      for (let i = 0; i < arr.length; i++) {
         if (arr[i] === elementToSearch) {
            return i;
         }
      }
   
      return -1;
   }
   ``` 
  
   {{</toggle>}}

   {{<toggle title="Лінійний пошук, рекурсивний:">}}
   ```javascript
   const linearSearchRecursive = (arr, elementToSearch, from = 0, to = arr.length) => {
      if (from === to) {
         return -1;
      }
   
      return arr[from] === elementToSearch
              ? from
              : linearSearchRecursive(arr, elementToSearch, from + 1, to);
   }   
   ```
   {{</toggle>}}

   {{<toggle title="Binary Search">}}

   ```javascript
   const binarySearch = (arr, elementToSearch) => {
    let left = 0;
    let right = arr.length - 1;

    while (left <= right) {
        let mid = Math.floor((left + right) / 2);
        // if middle is search value - return it right away
        if (arr[mid] === elementToSearch) {
            return mid;

        } else if (arr[mid] < elementToSearch) {
            // if middle is LESS
            // move to the RIGHT part of array
            left = mid + 1;

        } else if (arr[mid] > elementToSearch) {
            // if middle is BIGGER
            // move to the LEFT part of array
            right = mid - 1;
        }
    }

       return -1;
    }  
   ```
   
   {{</toggle>}}

   {{<toggle title="Binary Search Recursive">}}

   ```javascript
   const binarySearchRecursive = (arr, elementToSearch, left = 0, right = arr.length) => {
    // termination check
    if (right >= left) {
        let mid = Math.floor(left + (right - left) / 2);

        // if middle is what we need - return index
        if (arr[mid] === elementToSearch) {
            return mid;
        }

        // if middle is BIGGER
        // call search recursively from LEFT part of array
        if (arr[mid] > elementToSearch) {
            return binarySearchRecursive(arr, elementToSearch, left, mid - 1);
        }

        // if middle is LESS
        // call search recursively from RIGHT part of array
        return binarySearchRecursive(arr, elementToSearch, mid + 1, right);
    }

      return -1;
   }
   ``` 
   
   {{</toggle>}}
   

   --------------
4. Алгоритми сортування. Частина 1.

   **Прості алгоритми сортування**
  {{<toggle title="Swap">}}

   ```javascript
   const swap = (arr, i, j) => {
       [arr[i], arr[j]] = [arr[j], arr[i]];
   }
   ```

   {{</toggle>}}
   {{<toggle title="Stupid Sort">}}
   
   ```javascript
   const stupidSort = (arr) => {
       let i = 0;
       while (i < arr.length - 1) {
           if (arr[i] <= arr[i + 1]) {
               i++;
           } else {
               swap(arr, i, i + 1);
               i = 0;
           }
       }
   }
   ```
   
   {{</toggle>}}
   {{<toggle title="Gnome Sort">}}
   
   ```javascript
   const gnomeSort = (arr) => {
       let i = 0;
       while (i < arr.length - 1) {
           if (arr[i] <= arr[i + 1]) {
               i++;
           } else {
               swap(arr, i, i + 1);
               // moving 1 step back only to recheck it, as we swapped element there
               if (--i === -1) {
                   i = 0;
               }
           }
       }
   }
   ```
   
   {{</toggle>}}
   {{<toggle title="Bubble Sort">}}
   
   ```javascript
   const bubbleSort = (arr) => {
       const n = arr.length;
       let swapped = false;
       for (let i = 0; i < n; i++) {
           swapped = false;
           for (let j = 0; j < n - i - 1; j++) {
               // large element will 'bubble up' to the end
               if (arr[j] > arr[j + 1]) {
                   swap(arr, j, j + 1);
                   swapped = true;
               }
           }
           if (!swapped) {
               break;
           }
       }
   }
   ```
   
   {{</toggle>}}
   {{<toggle title="Bubble Sort Recursive">}}
   
   ```javascript
   const bubbleSortRecursive = (arr, n) => {
       // Base case
       if (n === 1)
           return;
   
       for (let i = 0; i < n - 1; i++)
           if (arr[i] > arr[i + 1]) {
               swap(arr, i, i + 1);
           }
   
       // Largest element is fixed,
       // recur for remaining array
       bubbleSortRecursive(arr, n - 1);
   }
   ```
   
   {{</toggle>}}
   {{<toggle title="Cocktail Sort">}}
   
   ```javascript
   const cocktailSort = (arr) => {
       let left = 0;
       let right = arr.length;
   
       let swapped = true;
       while (swapped) {
   
           // 'bubble up' largest element to the end
           swapped = false;
           for (let i = left; i < right - 1; ++i) {
               if (arr[i] > arr[i + 1]) {
                   swap(arr, i, i + 1);
                   swapped = true;
               }
           }
           if (!swapped) {
               break;
           }
           // fix the last element
           right--;
   
           // 'bubble down' smallest element to the beginning (moving from right to left)
           swapped = false;
           for (let i = right - 1; i >= left; i--) {
               if (arr[i] > arr[i + 1]) {
                   swap(arr, i, i + 1);
                   swapped = true;
               }
           }
   
           // fix the first element
           left++;
       }
   }
   ```
   
   {{</toggle>}}
   {{<toggle title="Comb Sort">}}
   
   ```javascript
   const combSort = (arr) => {
       let n = arr.length;
       // init start gap
       let gap = n;
       let swapped = true;
   
       // Keep running while gap is more than 1 and last iteration caused a swap
       while (gap != 1 || swapped) {
   
           // Shrink gap by Shrink factor
           // this is a bit more efficient than using 1.247
           gap = Math.max(Math.floor((gap * 10) / 13), 1);
   
           swapped = false;
           // Compare all elements with current gap
           for (let i = 0; i < n - gap; i++) {
               if (arr[i] > arr[i + gap]) {
                   swap(arr, i, i + gap);
                   swapped = true;
               }
           }
       }
   }
   ```
   
   {{</toggle>}}
   {{<toggle title="Selection Sort">}}
   
   ```javascript
   const selectionSort = (arr) => {
       const n = arr.length;
       let pos;
       for (let i = 0; i < n; i++) {
           pos = i;
           // find the smallest element from the rest
           for (let j = i + 1; j < n; j++) {
               if (arr[j] < arr[pos])
                   pos = j;
           }
           // swap smallest from rest to the beginning
           swap(arr, i, pos);
       }
   }
   ```
   
   {{</toggle>}}
   {{<toggle title="Insertion Sort">}}
   
   ```javascript
   const insertionSort = (arr) => {
       let n = arr.length;
       for (let i = 1; i < n; i++) {
           let curr = arr[i];
           let j;
           // moving element into first 'sorted' part, maintaining sorted order
           for (j = i; j > 0; j--) {
               if (arr[j - 1] > curr) {
                   arr[j] = arr[j - 1];
               } else {
                   break;
               }
           }
           arr[j] = curr;
       }
   }
   ```
   
   {{</toggle>}}
   {{<toggle title="Shell Sort">}}
   
   ```javascript
   const shellSort = (arr) => {
       let h = 1;
       let n = arr.length;
       while (h < n) {
           h = h * 3 + 1;
       }
       h = Math.floor(h / 3);
       let c;
       let j;
   
       while (h > 0) {
           for (let i = h; i < n; i++) {
               c = arr[i];
               j = i;
               while (j >= h && arr[j - h] > c) {
                   arr[j] = arr[j - h];
                   j = j - h;
               }
               arr[j] = c;
           }
           h = Math.floor(h / 2);
       }
   }
   ```
   
   {{</toggle>}}

5. Алгоритми сортування. Частина 2.

   **Продвинуті алгоритми**
   {{<toggle title="Count Sort Simple">}}
   
   ```javascript
   // will fail with negative numbers
   const countSortSimple = (arr) => {
      const counter = new Array(1000).fill(0);
   
      for (const a of arr) {
         counter[a]++;
      }
   
      let i = 0;
      let j = 0;
      while (j < 1000) {
         if (counter[j] === 0) {
            j++;
         } else {
            counter[j]--;
            arr[i++] = j;
         }
      }
   }
   ```   
   
   {{</toggle>}}
   {{<toggle title="Count Sort">}}
   
   ```javascript
   // will work with negative numbers, but requires knowing min/max or finding them
   const countSort = (arr) => {
       let max = _.max(arr);
       let min = _.min(arr);
       let range = max - min + 1;
   
       const count = new Array(range).fill(0);
       const output = new Array(arr.length).fill(0);
       for (const a of arr) {
           count[a - min]++;
       }
   
       for (let i = 1; i < count.length; i++) {
           count[i] += count[i - 1];
       }
   
       for (let i = arr.length - 1; i >= 0; i--) {
           output[count[arr[i] - min] - 1] = arr[i];
           count[arr[i] - min]--;
       }
   
       // copy in sorted order to same array
       for(let i = 0; i < arr.length; i++) {
           arr[i] = output[i];
       }
   }
   ```   
   
   {{</toggle>}}
   {{<toggle title="Merge Sort Simple">}}
   
```javascript
// will work with negative numbers, but requires knowing min/max or finding them
const countSort = (arr) => {
    let max = _.max(arr);
    let min = _.min(arr);
    let range = max - min + 1;

    const count = new Array(range).fill(0);
    const output = new Array(arr.length).fill(0);
    for (const a of arr) {
        count[a - min]++;
    }

    for (let i = 1; i < count.length; i++) {
        count[i] += count[i - 1];
    }

    for (let i = arr.length - 1; i >= 0; i--) {
        output[count[arr[i] - min] - 1] = arr[i];
        count[arr[i] - min]--;
    }

    // copy in sorted order to same array
    for(let i = 0; i < arr.length; i++) {
        arr[i] = output[i];
    }
}
```   
   
   {{</toggle>}}
   {{<toggle title="Merge">}}
   
```javascript
const merge = (arr, low, mid, high) => {
    // Creating temporary subarrays
    const leftArray = new Array(mid - low + 1);
    const rightArray = new Array(high - mid);

    // Copying our subarrays into temporaries
    for (let i = low, j = 0; j < leftArray.length; i++, j++) {
        leftArray[j] = arr[i];
    }
    for (let i = mid + 1, j = 0; j < rightArray.length; i++, j++) {
        rightArray[j] = arr[i];
    }

    // Iterators containing current index of temp subarrays
    let leftIndex = 0;
    let rightIndex = 0;

    // Copying from leftArray and rightArray back into array
    for (let i = low; i < high + 1; i++) {
        // If there are still uncopied elements in R and L, copy minimum of the two
        if (leftIndex < leftArray.length && rightIndex < rightArray.length) {
            if (leftArray[leftIndex] < rightArray[rightIndex]) {
                arr[i] = leftArray[leftIndex];
                leftIndex++;
            } else {
                arr[i] = rightArray[rightIndex];
                rightIndex++;
            }
        } else if (leftIndex < leftArray.length) {
            // If all elements have been copied from rightArray, copy rest of leftArray
            arr[i] = leftArray[leftIndex];
            leftIndex++;
        } else if (rightIndex < rightArray.length) {
            // If all elements have been copied from leftArray, copy rest of rightArray
            arr[i] = rightArray[rightIndex];
            rightIndex++;
        }
    }
}
```   
   
   {{</toggle>}}
   {{<toggle title="Merge Sort">}}
   
```javascript
const mergeSort = (arr, low, high) => {
    low = low ?? 0;
    high = high ?? arr.length - 1;
    if (high <= low) return;

    let mid = Math.floor((low + high) / 2);
    mergeSort(arr, low, mid);
    mergeSort(arr, mid + 1, high);
    merge(arr, low, mid, high);
}
```   
   
   {{</toggle>}}
   {{<toggle title="Merge Sort Iterative">}}
   
```javascript
const mergeSortIterative = (arr) => {
    let low = 0;
    let high = arr.length - 1;

    // divide the array into blocks of size `m`
    // m = [1, 2, 4, 8, 16…]
    for (let m = 1; m <= high - low; m = 2 * m) {
        // for m = 1, i = 0, 2, 4, 6, 8 …
        // for m = 2, i = 0, 4, 8, 12 …
        // for m = 4, i = 0, 8, 16 …
        // …
        for (let i = low; i < high; i += 2 * m) {
            merge(arr, i, i + m - 1, Math.min(i + 2 * m - 1, high));
        }
    }
}
```   
   
   {{</toggle>}}
   {{<toggle title="Quick Sort">}}
   
```javascript
const quickSort = (arr, left, right) => {
    left = left ?? 0;
    right = right ?? arr.length - 1;
    if (left >= right)
        return;
    // divide array
    const p = partition(arr, left, right);
    // recursively work with left and right part of divided array
    quickSort(arr, left, p - 1);
    quickSort(arr, p + 1, right);
}
```   
   
   {{</toggle>}}
   {{<toggle title="Quick Sort Iterative">}}
   
```javascript
const quickSortIterative = (arr, left, right) => {
    left = left ?? 0;
    right = right ?? arr.length - 1;

    //auxiliary stack
    const intStack = new Array(right - left + 1);

    // top of stack initialized to -1
    let top = -1;

    // push initial values of left and right to stack
    intStack[++top] = left;
    intStack[++top] = right;

    // Keep popping from stack while is not empty
    while (top >= 0) {
        right = intStack[top--];
        left = intStack[top--];

        // Set pivot element at its correct position
        // in sorted array
        const pivot = partition(arr, left, right);

        // If there are elements on left side of pivot,
        // then push left side to stack
        if (pivot - 1 > left) {
            intStack[++top] = left;
            intStack[++top] = pivot - 1;
        }

        // If there are elements on right side of pivot,
        // then push right side to stack
        if (pivot + 1 < right) {
            intStack[++top] = pivot + 1;
            intStack[++top] = right;
        }
    }
}
```   
   
   {{</toggle>}}
   {{<toggle title="Partition, Swap">}}
   
```javascript
/**
 * For simplicity, this function takes the last element as the pivot.
 * Then, checks each element and swaps it before the pivot if its value is smaller.
 *
 * By the end of the partitioning, all elements less than the pivot are on the left of it
 * and all elements greater than the pivot are on the right of it.
 * The pivot is at its final sorted position and the function returns this position
 */
const partition = (arr, left, right) => {
    const pivot = arr[right];
    let j = left;
    for (let i = left; i <= right; i++) {
        if (arr[i] < pivot) {
            swap(arr, i, j);
            j++;
        }
    }
    swap(arr, right, j);

    return j;
}

const swap = (arr, i, j) => {
    [arr[i], arr[j]] = [arr[j], arr[i]];
}
```   
   
   {{</toggle>}}

6. Дерева
{{<toggle title="Binary Search Tree">}}

```javascript
const main = () => {
    const bt = new BinarySearchTree();

    bt.add(6);
    bt.add(4);
    bt.add(8);
    bt.add(3);
    bt.add(5);
    bt.add(7);
    bt.add(1);
    bt.add(9);
    bt.add(2);

    /*
                           6
                      4         8
                  3      5    7   9
                1
                  2
    */

    bt.traverseInOrder();
    bt.traverseLevelOrder();

    bt.delete(3);

    /*
                        6
                   4         8
                1    5    7    9
                 2
     */

    bt.traverseLevelOrder();

    bt.delete(6);
    bt.traverseLevelOrder();
    /*
                        7
                   4         8
                1    5          9
                 2
     */
}

class TreeNode {
    constructor(value) {
        this.value = value;
        this.right = null;
        this.left = null;
    }
}

class BinarySearchTree {

    constructor() {
        this.root = null;
    }

    add(value) {
        this.root = this.addRecursive(this.root, value);
    }

    addRecursive(current, value) {
        // if empty place found -- insert here
        if (current == null) {
            return new TreeNode(value);
        }

        // checking new value against current
        // and recursively going down to left or right
        if (value < current.value) {
            current.left = this.addRecursive(current.left, value);
        } else if (value > current.value) {
            current.right = this.addRecursive(current.right, value);
        } else {
            // value already exists
            return current;
        }

        return current;
    }

    containsNode(value) {
        return this.containsNodeRecursive(this.root, value);
    }

    containsNodeRecursive(current, value) {
        // we got to the end without value -- thus it is not in tree
        if (current == null) {
            return false;
        }
        // found it
        if (value === current.value) {
            return true;
        }

        // checking it according to rules
        // and recursively moving to left or right branch
        return value < current.value
            ? this.containsNodeRecursive(current.left, value)
            : this.containsNodeRecursive(current.right, value);
    }

    delete(value) {
        this.root = this.deleteRecursive(this.root, value);
    }

    deleteRecursive(current, value) {
        // we got to the end without needed node -- nothing to delete
        if (current == null) {
            return null;
        }

        // found node to delete
        if (value === current.value) {
            return this.deleteNode(current);
        }

        // moving recursively either left or right based on value, trying to find the node to delete
        if (value < current.value) {
            current.left = this.deleteRecursive(current.left, value);
            return current;
        }
        current.right = this.deleteRecursive(current.right, value);
        return current;
    }

    deleteNode(node) {
        console.log("REMOVING: " + node.value);
        // if there are no child nodes -- it can be easily removed
        if (node.left == null && node.right == null) {
            return null;
        }

        // if there is only one child -- it will be put on the deleted place
        if (node.right == null) {
            return node.left;
        }
        if (node.left == null) {
            return node.right;
        }

        // otherwise, we have both child nodes, thus need to find smallest node from right subtree
        // and put that node on the place of deleted node
        const smallestValue = this.findSmallestValueRecursive(node.right);
        // update node with smallest value
        node.value = smallestValue;
        // now we need to remove that found smallest node, because it is now on the other place
        node.right = this.deleteRecursive(node.right, smallestValue);
        return node;
    }

    findSmallestValue() {
        return this.findSmallestValueRecursive(this.root);
    }

    findSmallestValueRecursive(root) {
        // moving left as much as possible. last element is smallest
        return root.left == null ? root.value : this.findSmallestValueRecursive(root.left);
    }

    findGreatestValue() {
        return this.findGreatestValueRecursive(this.root);
    }

    findGreatestValueRecursive(root) {
        // moving right as much as possible. last element is greatest
        return root.right == null ? root.value : this.findGreatestValueRecursive(root.right);
    }

    traverseInOrder() {
        this.traverseInOrderRecursive(this.root);
        process.stdout.write("\n");
    }

    traverseInOrderRecursive(node) {
        // visiting left node recursively
        // visiting current node
        // visiting right node recursively
        if (node != null) {
            this.traverseInOrderRecursive(node.left);
            process.stdout.write(" " + node.value);
            this.traverseInOrderRecursive(node.right);
        }
    }

    traversePreOrder() {
        this.traversePreOrderRecursive(this.root);
        process.stdout.write("\n");
    }

    traversePreOrderRecursive(node) {
        // visiting current node
        // visiting left node recursively
        // visiting right node recursively
        if (node != null) {
            process.stdout.write(" " + node.value);
            this.traversePreOrderRecursive(node.left);
            this.traversePreOrderRecursive(node.right);
        }
    }

    traversePostOrder() {
        this.traversePostOrderRecursive(this.root);
        process.stdout.write("\n");
    }

    traversePostOrderRecursive(node) {
        // visiting left node recursively
        // visiting right node recursively
        // visiting current node
        if (node != null) {
            this.traversePostOrderRecursive(node.left);
            this.traversePostOrderRecursive(node.right);
            process.stdout.write(" " + node.value);
        }
    }

    traverseLevelOrder() {
        // visiting nodes level by level
        if (this.root == null) {
            return;
        }

        // storing child nodes here
        const nodes = [];
        nodes.push(this.root);

        // visiting all of nodes left in queue
        while (nodes.length > 0) {

            const node = nodes.shift();

            process.stdout.write(" " + node.value);

            // adding child nodes to the queue (they will be last)
            if (node.left != null) {
                nodes.push(node.left);
            }

            if (node.right != null) {
                nodes.push(node.right);
            }
        }

        process.stdout.write("\n");
    }
}

main();
```

{{</toggle>}}


