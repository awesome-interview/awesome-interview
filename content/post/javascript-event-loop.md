---
title: JavaScript Event Loop
subtitle: Event Loop
date: 2022-06-17
tags: ["example", "markdown"]
---

[The Node.js Event Loop, Timers, and process.nextTick()](https://nodejs.org/ru/docs/guides/event-loop-timers-and-nexttick/) \
[Don’t Block the Event Loop (or the Worker Pool)](https://nodejs.org/en/docs/guides/dont-block-the-event-loop/)
[What you should know to really understand the Node.js Event Loop](https://medium.com/the-node-js-collection/what-you-should-know-to-really-understand-the-node-js-event-loop-and-its-metrics-c4907b19da4c) \
[Знай свой инструмент: Event Loop в libuv](https://habr.com/ru/post/336498/) \
[Node.JS Architecture — Tutorial](https://www.vskills.in/certification/tutorial/node-js-architecture-2/) \
[New Changes to the Timers and Microtasks in Node v11.0.0 ( and above)](https://blog.insiderattack.net/new-changes-to-timers-and-microtasks-from-node-v11-0-0-and-above-68d112743eb3?gi=2e4235447cfd)

### Як працює Event Loop в NodeJS: внутрішня будова, фази та приклади
-----
#### Внутрішня будова NodeJS

![Event Loop](/images/loop1.png)

{{<indent-sm>}}

Реалізація Event Loop знаходиться в бібліотеці libuv, яка разом з V8 є основою NodeJS. Node не працює за принципом
виділення окремого потоку для кожного запиту, натомість, весь JavaScript код виконується в єдиному потоці, в якому
опрацьовується цикл подій.

Операційні системи надають інтерфейси для роботи з I/O операціями асинхронно, які libuv за можливості використовує
замість окремих потоків, так само виконується робота з базами даних та іншими системами. Бібліотека libuv створює пул
потоків для виконання асинхронних операцій, які використовуються тільки чотирма вбудованими частинами NodeJS, а саме
модулями fs, crypto, zlib та для DNS-пошуку. Всі інші операції використовують основний потік, в якому опрацьовується
цикл подій.

При старті, Node ініціалізовує Event Loop, виконує початковий код, що запускає таймери, робить запити і т.д., а тоді
переходить до опрацювання циклу подій.

{{</indent-sm>}}

#### Фази Event Loop

![Event Loop](/images/loop2.png)

{{<indent-sm>}}

Щоб розуміти Event Loop потрібно розуміти які є фази та що відбувається на кожній з них. В libuv є 7 фаз:

   ```
   timers
   pending callback
   idle
   prepare
   poll
   check
   close callbacks
   ```

Дві фази не використовуються в NodeJS, а саме idle та prepare.

Кожна з фаз представляє собою FIFO-чергу колбеків, коли Event Loop переходить в конкретну фазу, він виконує операції
пов’язані з нею та колбеки в черзі, поки вони не закінчаться або поки максимальна їх кількість не виконається, після
цього він переходить до наступної фази.

Чим є кожна з фаз? {{</indent-sm>}}

#### timers

{{<indent-sm>}}

На цій фазі запускається код, запланований за допомогою **setTimeout()** та **setInterval()**. Це відбудеться якомога
швидше після заданого проміжку часу, однак операційна система або інші колбеки можуть його затримати. NodeJS тільки
гарантує, що він виконається не раніше заданого таймауту. Тобто запис **setTimeout(() => console.log('log'), 1000)**
означає, що колбек виконається після закінчення 1000 мілісекунд, це може бути 1004мс, 1100мс або навіть більше.

{{</indent-sm>}}

-------

#### pending callbacks

{{<indent-sm>}}

На цій фазі виконуються I/O колбеки, відкладені з попередньої ітерації циклу. Наприклад, помилки, колбеки яких не були
виконані раніше через спроби системи їх виправити. Так, помилки ECONNREFUSED повідомлення TCP сокету на деяких *nix
системах можуть будуть опрацьовані на цій фазі.

{{</indent-sm>}}

-------

#### idle, prepare
{{<indent-sm>}}

В Node не використовуються. На цій фазі виконуються внутрішні операції libuv та підготовка до poll фази.

{{</indent-sm>}}

----
#### poll

{{<indent-sm>}}

Фаза poll розраховує на скільки вона повинна заблокувати Event Loop і очікувати події та, власне, опрацьовує ці події.

Якщо Event Loop перейшов в poll фазу і немає коду, запланованого за допомогою **setImmediate()**, то буде виконано її колбеки, поки вони не закінчаться або поки не досягнеться їх ліміт. Якщо черга пуста, Event Loop буде очікувати на нові події і після цього одразу їх опрацює.

Якщо Event Loop перейшов в poll фазу і її черга пуста, але раніше викликався **setImmediate()**, то poll фаза буде пропущена і цикл одразу перейде до фази check.

{{</indent-sm>}}

-----
#### check
{{<indent-sm>}}

На цій фазі виконується код, запланований за допомогою setImmediate(). Це дозволяє виконати певний код одразу після poll фази. Коли фаза poll стає неактивною і є код, поставлений в чергу за допомогою setImmediate(), Event Loop перейде в check фазу, замість очікування нових I/O подій.

{{</indent-sm>}}

-----

#### close callbacks
{{<indent-sm>}}

Остання фаза, протягом якої виконуються закриваючі колбеки, наприклад, socket.on('close'). Якщо обробник або сокет закрився неочікувано, на цій фазі буде виконано подію close, в іншому випадку вона буде запущена через process.nextTick().

Між кожною ітерацію циклу Node перевіряє чи очікуються нові події — якщо ні, то процес завершується.


{{</indent-sm>}}

-----

#### setTimeout(), setInterval(), setImmediate(), process.nextTick(), Promise
{{<indent-sm>}}

В NodeJS є п’ять способів безпосередньої роботи з порядком виконання коду: setTimeout(), setInterval(),setImmediate(), process.nextTick()та Promise.


{{</indent-sm>}}

#### setTimeout(), setInterval()
{{<indent-sm>}}

Код, запланований за допомогою цих двох функцій, виконується після заданого проміжку часу на фазі timers. Різниця між цими двома функціями тільки в тому, що setInterval() після виконання буде заплановано знову. Вони корисні, коли потрібно виконати код через заданий проміжок часу або виконувати його з певним інтервалом.


{{</indent-sm>}}

#### setImmediate()
{{<indent-sm>}}

Функція setImmediate дозволяє запланувати код, який виконається одразу після фази poll на check фазі. Оскільки setImmediate()та setTimeout()/setInterval() виконуються на різних фазах циклу, їх порядок залежить від контексту виконання. Якщо обидві функції викликаються з головного модуля, їх порядок залежить від продуктивності процесу, на який можуть впливати інші запущені процеси.

Тобто, якщо ми запустимо код, що не знаходиться в I/O циклі, порядок може бути непостійним:
```javascript
// eventloop.js

setTimeout(() => console.log('setInterval'));
setImmediate(() => console.log('setImmediate'));

$ node eventloop.js
setImmediate
setInterval

$ node eventloop.js
setInterval
setImmediate
```

Так стається, тому що колбек фази timers може бути пропущений на першій ітерації циклу подій. Коли код планується за допомогою setTimeout(callback), цей виклик внутрішньо перетворюється в setTimeout(callback, 1). Тому якщо підготовка перед першою ітерацією циклу займе більше 1мс, то цей колбек виконається на першій ітерації, якщо ж пройде менше 1мс, цей колбек буде виконано на наступній фазі.

Якщо ж код буде викликатися в I/O циклі, то колбек setImmediate() завжди буде викликатися першим.

```javascript
// eventloop.js

const fs = require('fs');

fs.readFile(__filename, () => {
  setTimeout(() => console.log('setInterval'));
  setImmediate(() => console.log('setImmediate'));
});

$ node eventloop.js
setImmediate
setInterval

$ node eventloop.js
setImmediate
setInterval
```

В прикладі вище порядок постійний тому що колбеки читання з файлу виконуються на фазі poll, за якою слідує check фаза, на якій виконується код, запланований за допомогою setImmediate().

Сама функція setImmediate() буває корисна, якщо потрібно виконати код до опрацювання I/O подій, що знаходяться в черзі, оскільки якщо при переході в poll фазу, черга check непорожня, poll буде пропущена, або коли потрібно виконати код одразу після I/O подій, що вже опрацьовуються.


{{</indent-sm>}}

#### procces.nextTick()

{{<indent-sm>}}

Метод process.nextTick() технічно не є частиною libuv Event Loop, натомість код, запланований за допомогою цього методу, буде виконано після поточної операції, незалежно від фази циклу. Зробивши рекурсивні виклики process.nextTick(), можна заблокувати Event Loop, тому варто використовувати цей метод обережно.

Цей метод може бути корисним, якщо потрібно виконати код одразу після поточного, наприклад, викликати якусь подію після додавання всіх обробників.

В прикладі нижче, колбек події constructed не буде викликано:

```javascript
// eventloop.js

const { EventEmitter } = require('events');

class MyEmitter extends EventEmitter {
  constructor() {
    super();
    this.emit('constructed');
  }
}
  
const emitter = new MyEmitter();
  
emitter.on('constructed', () => {
  console.log('constructed');
});

$ node eventloop.js
$ node eventloop.js
$
```
Натомість наступний код спрацює:
```javascript
// eventloop.js

const { EventEmitter } = require('events');

class MyEmitter extends EventEmitter {
  constructor() {
    super();

    process.nextTick(() => {
        this.emit('created');
    });
  }
}
  
const emitter = new MyEmitter();
 
emitter.on('created', () => {
  console.log('created');
});

$ node eventloop.js
created


$ node eventloop.js
created
```

В NodeJS до версії 11, черга process.nextTick() опрацьовувалася між фазами Event Loop. Але зараз, у версії 11+, код, запланований за допомогою process.nextTick(), буде виконано між колбеками поточної черги фази циклу подій:

```javascript
// eventloop.js

setTimeout(() => console.log('timeout1'));
setTimeout(() => {
  console.log('timeout2')
  process.nextTick(() => console.log('process.nextTick()')); 
});
setTimeout(() => console.log('timeout3'));
```

```
$ node eventloop.js
timeout1
timeout2
process.nextTick()
timeout3
```

Раніше, цей код вивів б в консоль наступне:
```
$ node eventloop.js
timeout1
timeout2
timeout3
process.nextTick()
```
{{</indent-sm>}}

#### Promise
{{<indent-sm>}}

В NodeJS також є черга мікротасок, її колбеки запускаються одразу після колбеків process.nextTick(), саме в цю чергу і попадають колбеки промісів:
```javascript
// eventloop.js

setTimeout(() => console.log('timeout1'));
setTimeout(() => {
  console.log('timeout2')
  Promise.resolve()
    .then(() => console.log('promise'));
  process.nextTick(() => console.log('process.nextTick()')); 
});
setTimeout(() => console.log('timeout3'));
```

```
$ node eventloop.js
timeout1
timeout2
process.nextTick()
promise
timeout3
```

{{</indent-sm>}}

#### Загальний приклад
{{<indent-sm>}}

```javascript
// eventloop.js

const fs = require('fs');

fs.readFile(__filename, () => {
  console.log('readFile');

  setTimeout(() => console.log('timeout1'));
  setImmediate(() => console.log('immediate1'));
  Promise.resolve()
    .then(() => console.log('Promise.resolve1'));
  process.nextTick(() => console.log('process.nextTick1'));
});

setTimeout(() => console.log('timeout2'));
setImmediate(() => console.log('immediate2'));
Promise.resolve()
  .then(() => console.log('Promise.resolve2'));
process.nextTick(() => console.log('process.nextTick2'));

console.log('sync code');

```

```
$ node eventloop.js
sync code
process.nextTick2
Promise.resolve2
timeout2
immediate2
readFile
process.nextTick1
Promise.resolve1
immediate1
timeout1
```

Чому порядок саме такий?

На початку, NodeJS виконує весь синхронний код, тому sync code повідомлення вивелось в консоль першим, перед цим у відповідні черги попали попередні колбеки. Після виконання синхронного коду NodeJS почав опрацьовувати Event Loop, оскільки в черзі process.nextTick() та черзі мікротасок були колбеки, вони одразу виконалися. Опісля, цикл подій перейшов у фазу timers та було виведено повідомлення timeout2. Перейшовши в фазу poll колбек читання файлу не буде виконано, бо черга фази check непорожня, тому її буде опрацьовано, пропустивши фазу poll.

На другій ітерації циклу більше немає інших колбеків, окрім readFile, який буде опрацьовано на poll фазі, тут і будуть додані нові колбеки. Після цього, будуть опрацьовані колбеки черги nextTick() та мікротасок, і цикл перейде у фазу check, де буде виведено повідомленн immediate1. На третій ітерації циклу, буде виведено повідомлення timeout1.

Після третьої фази, так як більше немає запланованих операцій, процес буде завершено.

{{</indent-sm>}}

[Фази Event Loop]({{< ref "#фази-event-loop" >}} "Фази Event Loop")

[Загальний приклад]({{< ref "#загальний-приклад" >}} "Загальний приклад")

[Як працює Event Loop в NodeJS: внутрішня будова, фази та приклади]({{< ref "#як-працює-event-loop-в-nodejs-внутрішня-будова-фази-та-приклади" >}} "Як працює Event Loop в NodeJS: внутрішня будова, фази та приклади")

   


