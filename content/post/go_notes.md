---
title: Go Notes
subtitle: Конспект по Go
date: 2022-06-19
tags: ["go"]
---

### Data Structures

In this chapter is about Go’s data structures and the mechanical sympathy behind them.

#### CPU Caches
{{<indent-sm>}}

There are lots of mechanical differences between processors and their design. 
In this section, I will talk at a high level about processors and the semantics that are relatively the same between them all. This semantic understanding will provide me a good mental model 
for how the processor works and the sympathy I can provide.
Each core inside the processor has its own local cache of memory (L1 and L2) 
and a common cache of memory (L3) used to store/access data and instructions. 
The hardware threads in each core can access their local L1 and L2 caches. 
Data from L3 or main memory needs to be copied into the L1 or L2 cache for access.

![Go Strings](/images/go/fig3-1.png)

The latency cost of accessing data that exists in the different caches changes from 
least to most: L1 -> L2 -> L3 -> main memory. As Scott Meyers said, 
"If performance matters then the total amount of memory I have is the total amount 
of cache. 
Main memory is so slow to access, practically speaking, it might as well not
even be there."

![Go Strings](/images/go/list3-1.png)

How do I write code that guarantees the data that is needed to execute an 
instruction is always present in the L1 or L2 caches? I need to write code 
that is mechanically sympathetic with the processor’s prefetcher. 
The prefetcher attempts to predict what data is needed before instructions request 
the data so it’s already present in either the L1 or L2 cache.

There are different granularities of memory access depending on where the access 
is happening. My code can read/write a byte of memory as the smallest unit 
of memory access. However, from the caching systems point of view, the granularity 
is 64 bytes. This 64 byte block of memory is called a cache line.

The Prefetcher works best when the instructions being executed create predictable 
access patterns to memory. One way to create a predictable access pattern to memory 
is to construct a contiguous block of memory and then 
iterate over that memory performing a linear traversal with a predictable stride.

The array is the most important data structure to the hardware because it supports 
predictable access patterns. However, the slice is the most important data structure 
in Go. Slices in Go use an array underneath.

Once I construct an array, every element is equally distant from the next or 
previous element. As I iterate over an array, I begin to walk cache line by 
connected cache line in a predictable stride. The Prefetcher will pick up on 
this predictable data access pattern and begin to efficiently pull the data into 
the processor, thus reducing data access latency costs.

Imagine I have a big square matrix of memory and a linked list of nodes that 
match the number of elements in the matrix. If I perform a traversal across the 
linked list, and then traverse the matrix in both directions (Column and Row), 
how will the performance of the different traversals compare?

```
func RowTraverse() int {
	var ctr int
	for row := 0; row < rows; row++ {
		for col := 0; col < cols; col++ {
			if matrix[row][col] == 0xFF {
				ctr++
			}
		}
	}
	return ctr
}
```

Row traverse will have the best performance because it walks through memory, 
cache line by connected cache line, which creates a predictable access pattern. Cache 
lines can be prefetched and copied into the L1 or L2 cache before the data is needed.

```
func ColumnTraverse() int {
	var ctr int
	for col := 0; col < cols; col++ {
		for row := 0; row < rows; row++ {
			if matrix[row][col] == 0xFF {
				ctr++
			}
		}
	}
	return ctr
}
```

Column Traverse is the worst by an order of magnitude because this access pattern 
crosses over OS page boundaries on each memory access. This causes no predictability 
for cache line prefetching and becomes essentially random access memory.


```
func LinkedListTraverse() int {
	var ctr int
	d := list
	for d != nil {
		if d.v == 0xFF {
			ctr++
		}
		d = d.p
	}
	return ctr
}
```

The linked list is twice as slow as the row traversal mainly because there are cache 
line misses but fewer TLB (Translation Lookaside Buffer) misses. 
A bulk of the nodes connected in the list exist inside the same OS pages.

![Go Strings](/images/go/list3-5.png)

I can see this is true from the benchmark results. 
I will learn about benchmarking later.

{{</indent-sm>}}

#### Translation Lookaside Buffer (TLB)
{{<indent-sm>}}

Each running program is given a full memory map of virtual memory by
the OS and that running program thinks they have all of the physical
memory on the machine. However, physical memory needs to be shared with
all the running programs. The operating system shares physical memory by
breaking the physical memory into pages and mapping pages to virtual memory
for any given running program. Each OS can decide the size of a page,
but 4k, 8k, 16k are reasonable and common sizes.

The TLB is a small cache inside the processor that helps to reduce
latency on translating a virtual address to a physical address within
the scope of an OS page and offset inside the page. A miss against the
TLB cache can cause large latencies because now the hardware has to wait
for the OS to scan its page table to locate the right page for the virtual
address in question. If the program is running on a virtual machine
(like in the cloud) then the virtual machine paging table needs to be scanned first.

Remember when I said:

The linked list is twice as slow as the row traversal mainly
because there are cache line misses but fewer TLB misses (explained next).
A bulk of the nodes connected in the list exist inside the same OS pages.

The LinkedList is orders of magnitude faster than the column traversal
because of TLB access. Even though there are cache line misses with the
linked list traversal, since a majority of the memory for a group of nodes
will land inside the same page, TLB latencies are not affecting performance.
This is why for programs that use a large amount of memory, like DNA based
applications, I may want to use a distribution of Linux that is configured
with page sizes in the order of a megabyte or two of memory.

All that said, data-oriented design matters. Writing an efficient algorithm
has to take into account how the data is accessed. Remember, performance today
is about how efficiently I can get data into the processor.

{{</indent-sm>}}

#### Declaring and Initializing Values
{{<indent-sm>}}

Declare an array of five strings initialized to its zero value state.

```
var strings [5]string
```

A string is an immutable, two word, data structure representing a pointer 
to a backing array
of bytes and the total number of bytes in the backing array. 
Since this array is set to its zero value state, every element is 
set to its zero value state. This means that each string has the first word 
set to nil and the second word set to 0.

![Go Strings](/images/go/fig3-2.png)

{{</indent-sm>}}

#### String Assignments
{{<indent-sm>}}

What happens when a string is assigned to another string?
```
strings[0] = "Apple"
```

When a string is assigned to another string, the two word value is copied,
resulting in two different string values both sharing the same backing array.

![Go Strings](/images/go/fig3-3.png)

The cost of copying a string is the same
regardless of the size of a string, a two word copy.

{{</indent-sm>}}

#### Iterating Over Collections
{{<indent-sm>}}

Go provides two different semantics for iterating over a collection.
I can iterate using value semantics or pointer semantics.

```
// Value Semantic Iteration
for i, fruit := range strings {
    println(i, fruit)
}

// Pointer Semantic Iteration
for i := range strings {
    println(i, strings[i])
}
```

When using value semantic iteration, two things happen. First, the collection 
I’m iterating over is copied and I iterate over the copy. In the case 
of an array, the copy could be expensive since the entire array is copied. 
In the case of a slice, there is no real cost since only the internal slice 
value is copied and not the backing array. Second, I get a copy of each element 
being iterated on.

When using pointer semantic iteration, I iterate over the original collection and 
I access each element associated with the collection directly.

{{</indent-sm>}}

#### Value Semantic Iteration
{{<indent-sm>}}

Given the following code and output.

```
strings := [5]string{"Apple", "Orange", "Banana", "Grape", "Plum"}

for i, fruit := range strings {
    println(i, fruit)
}

Output:
0 Apple
1 Orange
2 Banana
3 Grape
4 Plum
```

The strings variable is an array of 5 strings. 
The loop iterates over each string in the collection and displays the 
index position and the string value. Since this is value semantic iteration, 
the for range is iterating over its own shallow copy of the array and on each 
iteration the fruit variable is a copy of each string (the two word data structure).

Notice how the fruit variable is passed to the print function using value semantics. 
The print function is getting its own copy of the string value as well. By the time 
the string is passed to the print function, there are 4 copies of the string 
value (array, shallow copy, fruit variable and the print function’s copy). 
All 4 copies are sharing the same backing array of bytes.

![Go Strings](/images/go/fig3-4.png)

Making copies of the string value is important because it prevents 
the string value from ever escaping to the heap. 
This eliminates non-productive allocation on the heap.

{{</indent-sm>}}

#### Pointer Semantic Iteration
{{<indent-sm>}}

Given the following code and output.

```
strings := [5]string{"Apple", "Orange", "Banana", "Grape", "Plum"}

for i := range strings {
    println(i, strings[i])
}

Output:
0 Apple
1 Orange
2 Banana
3 Grape
4 Plum
```

Once again, the strings variable is an array of 5 strings. 
The loop iterates over each string in the collection and displays 
the index position and the string value. Since this is pointer semantic 
iteration, the for range is iterating over the strings array directly 
and on each iteration, 
the string value for each index position is accessed directly for the print call.

{{</indent-sm>}}

#### Data Semantic Guideline For Built-In Types
{{<indent-sm>}}

As a guideline, if the data I’m working with is a numeric, 
string, or bool, then use value semantics to move the data around my program. 
This includes declaring fields on a struct type.

```
func Foo(x int, y string, z bool) (int, string, bool)

type Foo struct {
	X int
	Y string
	Z bool
}
```

One reason I might take an exception and use pointer semantics is if 
I need the semantics of NULL (absence of value). Then using pointers of 
these types is an option, but document this if it’s not obvious.

The nice thing about using value semantics for these types is that 
I’m guaranteed that each function is operating on its own copy. 
This means reads and writes to this data are isolated to that function. 
This helps with integrity and identifying bugs related to data corruption.

{{</indent-sm>}}

#### Different Type Arrays
{{<indent-sm>}}

It’s interesting to see what the compiler provides as an error 
when assigning arrays of the same types that are of different lengths.

```
var five [5]int
four := [4]int{10, 20, 30, 40}

five = four

Compiler Error:
cannot use four (type [4]int) as type [5]int in assignment
```

{{</indent-sm>}}

#### Contiguous Memory Construction
{{<indent-sm>}}

I want to prove that an array provides a contiguous layout of memory.

```
five := [5]string{"Annie", "Betty", "Charley", "Doug", "Bill"}

for i, v := range five {
    fmt.Printf("Value[%s]\tAddress[%p]  IndexAddr[%p]\n", v, &v, &five[i])
}


Output:
Value[Annie]     Address[0xc000010250]    IndexAddr[0xc000052180]
Value[Betty]     Address[0xc000010250]    IndexAddr[0xc000052190]
Value[Charley]   Address[0xc000010250]    IndexAddr[0xc0000521a0]
Value[Doug]      Address[0xc000010250]    IndexAddr[0xc0000521b0]
Value[Bill]      Address[0xc000010250]    IndexAddr[0xc0000521c0]
```

Here I declare an array of 5 strings initialized with values. 
Then use value semantic iteration to display information about each string. 
The output shows each individual string value, the address of the v variable 
and the address of each element in the array.

I can see how the array is a contiguous block of memory and how a 
string is a two word or 16 byte data structure on my 64 bit architecture. 
The address for each element is distanced on a 16 byte stride.

The fact that the v variable has the same address on each iteration strengthens 
the understanding that v is a local variable of 
type string which contains a copy of each string value during iteration.

{{</indent-sm>}}


#### Constructing Slices
{{<indent-sm>}}

The slice is Go’s most important data structure
and it’s represented as a three word data structure.

![Go Strings](/images/go/fig3-5.png)

Constructing a slice can be done in several ways.

```
// Slice of string set to its zero value state.
var slice []string

// Slice of string set to its empty state.
slice := []string{}

// Slice of string set with a length and capacity of 5.
slice := make([]string, 5)

// Slice of string set with a length of 5 and capacity of 8.
slice := make([]string, 5, 8)

// Slice of string set with values with a length and capacity of 5.
slice := []string{"A", "B", "C", "D", "E"}
```

I can see the built-in function make allows me to pre-allocate both 
length and capacity for the backing array. If the compiler knows the size 
at compile time, the backing array could be constructed on the stack.

{{</indent-sm>}}

### Goroutines, channels
#### Горутини

{{<indent-sm>}}

Горутини - це ф-ї, які виконуються паралельно (це один з способів виконати ф-ю).
Наприклад, є ф-я **fmt.Println** - ми можемо її просто викликати, а можемо викликати так, щоб
вона виконалась не відразу, а колись пізніше, відкладено.
Горутини легковісні, в кожної з них є свій стек, все інше - пам"ять, файли - загальне.
Горутини дозволяють запускати будь-яку кількість дій одночасно.

Горутина призупиняється на очікування чого завгодно, коли виконується якась синхронізація,
якийсь мютекс, канал, waitgroup, коли горутина йде на якийсь системний виклик, наприклад
потрібно щось прочитати з файла, або з мережі, коли горутина просто спить.

When a Go program starts up, the Go runtime asks the machine (virtual or physical)
how many operating system threads can run in parallel. This is based on the number of
cores that are available to the program. For each thread that can be run in parallel,
the runtime creates an operating system thread (M) and attaches that to a data structure
that represents a logical processor (P) inside the program.
This P and M represent the compute power or execution context for running the Go program.

Also, an initial Goroutine (G) is created to manage the execution of
instructions on a selected M/P. Just like an M manages the execution of instructions
on the hardware, a G manages the execution of instructions on the M. This creates
a new layer of abstraction above the operating system, but it moves execution control
to the application level.

![Горутини](/images/go/goroutine.png)
[Что такое горутины и каков их размер?](https://habr.com/ru/company/otus/blog/527748/)

Кількість поточних горутин:
```go
package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Printf("Gouroutines: %d", runtime.NumGoroutine())
}
```
```
Gouroutines: 1
```

Наступний код не виведе нічого, бо main завершується до запуску горутини(горутина не встигла запуститися):
```go
package main

import "fmt"

func main() {
	go fmt.Printf("Hi")
}
```

Зараз горутина виконається:
```go
package main

import (
	"fmt"
	"time"
)

func main() {
	go fmt.Printf("Hi")
	time.Sleep(time.Second)
}
```
```
Hi
```

---
#### Замикання
Наступний код виведе скоріше за все всі 5, але, якщо процесорів більше одного, планувальник встигне запустити якусь горутину до закінчення циклу for, то з"явиться ще якась цифра: 
```go
package main

import (
	"fmt"
	"time"
)

func main() {
	for i := 0; i < 5; i++ {
		go func() {
			fmt.Println(i)
		}()
	}
	time.Sleep(2 * time.Second)
}
```
```
5
5
5
3
5

```
Пояснення - "i" це змінна цикла, вона існує в єдиному екземплярі, вона в циклі постійно оновлюється.
В нас функція(горутина) всередині цикла замикає "i" з зовнішнього скоупу, тобто бере адресу цієї змінної, тобто всередині горутини там покажчик на ту саму пам"ять, що і всередині цикла.\
Тотбто коли викликається **fmt.Println(i)** то буде взято те, що лежить за адресою змінної "i".

Можна "i" передати в якості аргументу: 
```go
package main

import (
	"fmt"
	"time"
)

func main() {
	for i := 0; i < 5; i++ {
		go func(j int) {
			fmt.Println(j)
		}(i)
	}
	time.Sleep(2 * time.Second)
}
```
```
4
3
0
2
1

```
Або для кожної ітерації об"являти свою змінну "i", яка перекриє змінну "i" самого циклу:
```go
package main

import (
	"fmt"
	"time"
)

func main() {
	for i := 0; i < 5; i++ {
		i := i
		go func() {
			fmt.Println(i)
		}()
	}
	time.Sleep(2 * time.Second)
}
```
```
4
3
2
0
1

```

{{</indent-sm>}}

---
#### Канали
#### Signaling and Channels
{{<indent-sm>}}

Channels allow Goroutines to communicate with each other through the use of signaling semantics. 
Channels accomplish this signaling through the use of sending/receiving data or by identifying state 
changes on individual channels. Don't architect software with the idea of channels being queues, 
focus on signaling and the semantics that simplify the orchestration required.

Depending on the problem I’m solving, I may require different channel semantics. 
Depending on the semantics I need, different architectural choices must be taken.

**Language Mechanics**
* Use channels to orchestrate and coordinate Goroutines.
  * Focus on the signaling semantics and not the sharing of data. 
  * Signaling with data or without data. 
  * Question their use for synchronizing access to shared state.
    * There are cases where channels can be simpler for this but initially question.
* Unbuffered channels:
  * Receive happens before the Send. 
  * Benefit: 100% guarantee the signal being sent has been received. 
  * Cost: Unknown latency on when the signal will be received.
* Buffered channels: 
  * Send happens before the Receive. 
  * Benefit: Reduce blocking latency between signaling. 
  * Cost: No guarantee when the signal being sent has been received.
    * The larger the buffer, the less guarantee.
* Closing channels:
  * Close happens before the Receive. (like Buffered)
  * Signaling without data. 
  * Perfect for signaling cancellations and deadlines.
* NIL channels:
  * Send and Receive block. 
  * Turn off signaling 
  * Perfect for rate limiting or short-term stoppages.

**Design Philosophy**
* If any given Send on a channel CAN cause the sending Goroutine to block:
  * Be careful with Buffered channels larger than 1.
    * Buffers larger than 1 must have reason/measurements.
  * Must know what happens when the sending Goroutine blocks.
* If any given Send on a channel WON'T cause the sending Goroutine to block:
  * I have the exact number of buffers for each send.
     * Fan Out pattern
  * I have the buffer measured for max capacity.
     * Drop pattern
* Less is more with buffers.
  * Don’t think about performance when thinking about buffers.
  * Buffers can help to reduce blocking latency between signaling.
    * Reducing blocking latency towards zero does not necessarily mean better throughput.
    * If a buffer of one is giving me good enough throughput then keep it.
    * Question buffers that are larger than one and measure for size.
    * Find the smallest buffer that provides good enough throughput.

**Channel Semantics**

It’s important to think of a channel not as a data structure, but as a mechanic for signaling. 
This goes in line with the idea that I send and receive from a channel, not read and write. 
If the problem in front of me can’t be solved with signaling, if the word signaling is not coming 
out of my mouth, I need to question the use of channels.

There are three things that I need to focus on when thinking about signaling. 
The first one is, does the Goroutine that is sending the signal, need a guarantee that 
the signal has been received? I might think that the answer to this question is always yes, 
but remember, there is a cost to every decision and there is a cost to having a guarantee 
at the signaling level.

The cost of having the guarantee at the signaling level is unknown latency. 
The sender won’t know how long they need to wait for the receiver to accept the signal. 
Having to wait for the receiver creates blocking latency. In this case, unknown amounts 
of blocking latency. The sender has to wait, for an unknown amount of time, until the 
receiver becomes available to receive the signal.

Waiting for the receiver means mechanically, the receive operation happens before the send. 
With channels, the receive happens nanoseconds before, but it’s before. 
This means the receiver takes the signal and then walks away, allowing the sender
to now move on with a guarantee.

What if the process can’t wait for an unknown amount of time? What if that kind of latency won’t work? 
Then the guarantee can’t be at the signaling level, it needs to be outside of it. 
The mechanics behind this working is that the send now happens before the receive. 
The sender can perform the signal without needing the receiver to be available. 
So the sender gets to walk away and not wait. 
Eventually, I hope, the receiver shows up and takes the signal.

This is reducing latency cost on the send, but it’s creating uncertainty about 
signals being received and therefore knowing if there are problems upstream with receivers. 
This can create the process to accept work that never gets started or finished. 
It could eventually cause massive back pressure and systems to crash.

The second thing to focus on is, do I need to send data with the signal? 
If the signal requires the transmission of data, then the signaling is a 1 to 1 between Goroutines. 
If a new Goroutine needs to receive the signal as well, a second signal must be sent.

If data doesn’t need to be transmitted with the signal, then the signal can be a 1 to 1 or 1 
to many between Goroutines. Signaling without data is primarily used for cancellation or shutdowns. 
It’s done by closing the channel.

The third thing to focus on is channel state. A channel can be in 1 of 3 states.

A channel can be in a nil state by constructing the channel to its zero value state. 
Sends and receives against channels in this state will block. 
This is good for situations where I want to implement short term stoppages of work.

A channel can be in an open state by using the built-in function make. 
Sends and receives against channels in this state will work under the following conditions:

Unbuffered Channels:
Guarantees at the signaling level with the receive happening before send. 
Sending and receiving Goroutines need to come together in the same space and time 
for a signal to be processed.

Buffered Channels:
Guarantees outside of the signaling level with the send happening before the receive. 
If the buffer is not full, sends can complete else they block. If the buffer is not empty, 
receives can complete else they block.

A channel can be in a closed state by using the built-in function close. 
I don’t need to close a channel to release memory, this is for changing the state. 
Sending on a closed channel will cause a panic, however receiving on a closed channel will return immediately.

With all this information, I can focus on channel patterns. The focus on signaling is important. 
The idea is, if I need a guarantee at the signaling level or not, based on latency concerns. 
If I need to transmit data with the signal or not, based on handling cancellations or not. 
I want to convert the syntax to these semantics.

**Семантика**
```go
chan T
```

Канали працюють з конкретним типом\
Потокобезпечні\
Схожі на черги FIFO

Канал дозволяє безпечно передати дані з однієї горутини, в іншу.

---
**Операції з каналами**

Створити канал:
```go
ch := make(chan int)
```

* Різниця між **new** та **make**:  **new** - просто виділити пам"ять під щось(число ...),
  **make** - створити, сконструювати об"єкт(канал, map ...).

Відправити в канал:
```go
ср <- 10
```
Отримати з канала:
```go
v := <- ch
```
Закрити канал:
```go
close(ch)
```

* Декілька разів одне й те ж не можна отримати з каналу. Якщо щось отримали за каналу, то повторно його вже не можна отримати.

---
**Буферизований канал**
```go
ch := make(chan int, 4)
```
![Канали](/images/go/buff_chan.png)
Є горутина яка отримує(зліва) з канала, та є горутина яка відправляє(зправа) в канал.
Відправляти в канал можна поки є в ньому місце(4).

---
**Небуферизований канал**
```go
ch := make(chan int)
```
![Канали](/images/go/not_buff_chan.png)
В небуферизованому каналі дані передаються "з рук в руки", бо буфер = 0.
В небуферизований канал не можна відправити дані(потрібно чекати) поки ресівер не прийде і не скаже, "а що можна отримати з каналу?".

---
**Що буде, якщо отримувати з пустого каналу?**
![Канали](/images/go/receive_empty_chan.png)
Є канал з буфером 4, але він пустий.
Що буде з горутиною, яка намагається щось отримати з каналу? - Горутина буде чекати
поки щось буде відправлено в канал.
![Канали](/images/go/waight_empty_chan.png)

---
**Що буде, якщо відправляти в заповнений канал?**
![Канали](/images/go/send_to_full_chan.png)
Якщо е канал з буфером 4, але він повний, то горутина яка відправляє в канал,
буде чекати поки з"явиться вільне місце.
![Канали](/images/go/send_to_full_chan_wait.png)

---
**Що буде, якщо відправляти в закритий канал?**
![Канали](/images/go/send_to_closed_chan.png)
Якщо горутина буде намагатися відправити щось в закритий канал, буде паніка.
Тому, що Go панікує, коли нема іншої адекватної поведінки.
Закритий канал відкрити не можна.
![Канали](/images/go/send_to_closed_chan_panic.png)

---
**Що буде, якщо намагатися отримувати з закритого каналу?**
![Канали](/images/go/receive_closed_chan.png)
Є канал, він закритий і в ньому є дані. Ми хочемо їх отримати.
В цьому випадку горутина отримає по одному всі значення.
![Канали](/images/go/receive_from_closed_chan1.png)

---
**Що буде, якщо намагатися отримувати з закритого пустого каналу?**
![Канали](/images/go/receive_from_closed_empty_chan.png)
Є канал, він закритий і в ньому немає даних.
В цьому випадку горутина не буде чекати, просто буде працювати далі(отримає zero val).
![Канали](/images/go/receive_from_cl_chann.png)

{{</indent-sm>}}

#### Синхронизація горутин каналами
{{<indent-sm>}}
Є канал без буфера(потрібно щоб хтось зразу отримав дані з каналу) з типом пустої структури.
В **main** ми отримуємо з каналу, в горутині ми відправляємо в канал.
```go
package main

import (
	"fmt"
)

func main() {
	var ch = make(chan struct{})

	go func() {
		fmt.Printf("Hello")
		ch <- struct{}{}
	}()

	<-ch
}
```
Вивід:
```
"Hello"
```
"Hello" надрукувалось тому, що **main**(отримувач(ресівер) з каналу) зупинилась і чекала коли 
горутина(сендер) виконається і відправе структуру в канал.

---
В **main** ми відправляємо в канал, в горутині ми отримуємо з каналу.
```go
package main

import (
  "fmt"
)

func main() {
  var ch = make(chan struct{})

  go func() {
    fmt.Printf("Hello again")
    <-ch
  }()

  ch <- struct{}{}
}
```
Вивід:
```
"Hello again"
```
"Hello again" надрукувалось тому, що **main**(сендер в канал) зупинилась і чекає(бо канал небуферизований) коли
горутина(ресівер) виконається і отримає дані з каналу.

---
**Отримання з каналу поки він не закритий**
Значення та флаг - чи можуть ще з"явитися значення:
```go
v, ok := ch
```
Якщо ми читаємо з закритого каналу, і він вже пустий, ми отримаємо zero value. 
Але ми могли записати і пусті значення в канал. Щоб відрізнити, що ми отримали саме значення з каналу
котре записали як zero value, від того, що ми прочитали з каналу zero value тому, що в ньому вже нічого
нема - існує синтаксис вище, а саме в змінну "ok" записується флаг чи є ще в каналі значення
чи він вже пустий.

---
**Паттерн Producer-Consumer**

Producer:
```go
for _, t := range tasks {
	ch <- t
}
close(ch)
```

Consumer:
```go
for {
	x, ok := <-ch
	if !ok {
		break
	}

	fmt.Println(x)
}

// Або спрощений синтаксис:
for x := range ch {
    fmt.Println(x)
}
```
Є код, який створює таски і відправляє в канал, і потім його закриває як таски закінчились 
і є інший код, який обробляє ці таски. І коли код який обробляє таски відправлені в канал
отримує сигнал "!ok" що таски закінчились, він зупиняє обробку "break".
**for x := range ch** - означає прочитати всі значення в каналі.

---
**Хто закриває канал?**

Канал закриває той, хто в нього відправляє, бо якщо відправити в закритий канал, то буде паніка.
Якщо відправників декілька, то закриває той, хто створив цих відправників.

* Якщо закрити закритий канал - то буде паніка.

---
**Можна створити однонаправлений канал**

```go
chan <- T // тільки відправка
<- chan T // тільки отримання
```

Наприклад код не скомпілюється:

```go
package main

func fn(out chan<- int) {
	<-out
}

func main() {
	var ch = make(chan int)
	fn(ch)
}
```

буде помилка:
```
invalid operation: cannot receive from send-only channel out (variable of type chan<- int)
```

{{</indent-sm>}}

#### Канали: мультиплексування
{{<indent-sm>}}

Для роботи з декількома каналами існує select:

```go
select {
case x := <-ch1:
	// ...
case ch2 <- y:
	// ...
default:
    // ...
}
```

коли код доходить до select, то він чекає коли можна щось отримати з ch1 або
коли можна щось відправити в ch2, і коли щось з них з"явиться, той case і виконається.
Якщо можна і читати з ch1 і писати в ch2, то гілка case виконається рандомно, або на ch1 або на ch2.
Коли ввели default, то коли жоден з каналів не готовий, то буде виконаний default.

**Канали: таймаут** 

Приклад використання select:
```go
package main

import (
  "fmt"
  "time"
)

func main() {
  ch := make(chan string)
  timer := time.NewTimer(10 * time.Second)
  select {
  case data := <-ch:
    fmt.Printf("received: %v", data)
  case <-timer.C:
    fmt.Printf("failed to receive in 10s")
  }
}

```
Код працює так - або ми отримаєм результат в гілці **case data := <-ch**,
або якщо ми його не отримаєм раніше ніж 10сек, то спрацює гілка з таймером **case <-timer.C**.

---
**Канали: періодик**

Код виглядає як звичайний таймаут підключитися до сервера. 
```go
package main

import (
	"fmt"
	"time"
)

func main() {
	doneCh := make(chan string)
	go func() {
		time.Sleep(2 * time.Second)
		close(doneCh)
	}()

	ticker := time.NewTicker(time.Second / 2)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			fmt.Println("tick")
		case v, ok := <-doneCh:          // case <-doneCh:
			fmt.Println("v, ok:", v, ok)  
		    return
		}
	}
}

```
Код виводить:
```
tick
tick
tick
tick
v, ok:  false

tick
tick
tick
tick

або

tick
tick
tick

```

---
**Канали: як сигнали(івенти)**
```
 make(chan struct{}, 1)
```

Джерело сгналу:
```
select {
    case notifyCh <- struct{}{}:
    default:
}
```

Приймач сигналу:
```
select {
    case <-notifyCh:
    case ...
}
```
Створюємо канал з буфером 1. Джерело відправляє структуру в канал.
Приймач сигналу може отримати відправлені дані.
Приймач може обробити тільки один івент.
Цей код як варіант обсервера.

---
**Канали: graceful shutdown**

```go
interruptCh := make(chan os.Signal, 1)

signal.Notify(interruptCh, os.Interrupt, syscall.SIGTERM)

fmt.Printf("Got %v...\n", <-interruptCh)
```
Створюємо канал і підписуємось на якісь івенти.
І Notify пише в канал interruptCh.
Див. код вище.

---
**Канали: патерн синхронізації**

Якщо якийсь кейс нам потрібен з більшим приоритетом, то пишем цикл і повторюєм 
його в двох "select"

```
for {
	select {
	    case <-quitCh:
            return
		default: 
	}
	
	select {
        case <-quitCh:
			return
        case <-ch1:
        // do smth
        case <-ch2:
        // do smth
    }
}
```

{{</indent-sm>}}

---
**Що виведе код?**
```go
package main

import "fmt"

func main() {
	ch := make(chan int)
	go func() {
		fmt.Println(7)
		ch <- 5
	}()
	fmt.Println(<-ch)
}
```

{{<toggle title="Відповідь:">}}

```
7
5
```

{{</toggle>}}

---
```go
package main

import "fmt"

func main() {
	ch := make(chan int, 3)
	ch <- 19
	ch <- 27
	ch <- 53
	close(ch)

	for v := range ch {
		fmt.Println(v)
	}
}
```

{{<toggle title="Відповідь:">}}

```
19
27
53
```

{{</toggle>}}

---
```go
package main

import "fmt"

func main() {
  ch1 := make(chan int, 10)
  ch1 <- 1

  ch2 := make(chan int, 10)
  ch2 <- 2

  select {
  case ch1 <- 3:
    fmt.Println("1")
  case <-ch2:
    fmt.Println("2")
  default:
    fmt.Println("3")
  }
}
```

{{<toggle title="Відповідь:">}}

```
1 або 2 
```

{{</toggle>}}


---
```go
package main

import "fmt"

func main() {
  ch := make(chan int, 10)
  ch <- 3
  close(ch)

  v, ok := <-ch
  fmt.Println(v)

  v, ok = <-ch
  fmt.Println(v)

  if ok {
    fmt.Println("!")
  }
}
```

{{<toggle title="Відповідь:">}}

```
3
0
```

{{</toggle>}}


